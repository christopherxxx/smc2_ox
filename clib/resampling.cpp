#include <oxexport.h>
#include <vector>
#include <stdio.h>
#include <iostream>                      // for std::cout

extern "C" void OXCALL FnDetResampling(OxVALUE *rtn, OxVALUE *pv, int cArg);

void OXCALL FnDetResampling(OxVALUE *rtn, OxVALUE *pv, int cArg)
{    
    int Nx;
    double u;
    
    // read variables
    OxLibCheckType(OX_MATRIX,pv,0,0);
    OxLibCheckType(OX_INT, pv, 1, 1);
    Nx = OxInt(pv, 1);
    OxLibCheckType(OX_DOUBLE,pv,2,2);
    u = OxDbl(pv,2);
    
    // return
    OxLibValMatMalloc(rtn, Nx, 1);

    int j = 0;
    double csw = OxMat(pv,0)[0][0];
    for(int k = 0; k < Nx; k++)
	{
	    while(csw < u)
		{
		    j++;
		    csw += OxMat(pv,0)[j][0];		    
		}
	    OxMat(rtn,0)[k][0] = j;
	    u = u + 1.0;
	}    
}

