#include <iostream>                      // for std::cout
#include <utility>                       // for std::pair
#include <boost/utility.hpp>             // for boost::tie
#include <boost/graph/graph_traits.hpp>  // for boost::graph_traits
#include <boost/graph/adjacency_list.hpp>


//using namespace std;

using namespace boost;
typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;
typedef std::pair<int, int> Edge;

Graph* callg()
{
    // 隣接リストを使って有向グラフを定義
    // typedef adjacency_list<vecS(node), vecS(edge), undirectedS> Graph; // 無向グラフは undirectedS


    enum 
    {
	A, B, C, D, E, N
    }
    ;


    Edge edge_array[] = 
	{
	    Edge(A,B), Edge(A,D), Edge(C,A), Edge(D,C), Edge(C,E), Edge(B,D), Edge(D,E), 
	}
    ;

    const int num_edges = sizeof(edge_array)/sizeof(edge_array[0]);

    const int num_vertices = N;


    // グラフオブジェクトを宣言
    // コンストラクタでエッジを与えてしまう
    // 引数は，エッジリストの開始ポインタと終点ポインタ，エッジ数
    Graph* g = new Graph(edge_array, edge_array + num_edges, num_vertices);
    

    // エッジイテレータのコンストラクタを使わずに，
    // オブジェクトを作成してから add_edge で辺を追加 する場合
    /*
          Graph g(num_vertices);
	      for (int i = 0; i < num_edges; ++i)
	            add_edge(edge_array[i].first, edge_array[i].second, g);
    */

    // 頂点インデックスのためのプロパティマップ

    return g;
}

int main(int argc, char* argv[])
{

    Graph* g=callg();
    Graph* g1=callg();
    
    const char* name = "ABCDE";
    typedef property_map<Graph, vertex_index_t>::type IndexMap;

    IndexMap index = get(vertex_index, *g);


    typedef graph_traits<Graph>::vertex_iterator vertex_iter;

    std::pair<vertex_iter, vertex_iter> vp;


    // 頂点リストの出力
    std::cout << "vertices(g) = ";

    for (vp = vertices(*g); vp.first != vp.second; ++vp.first)
	std::cout << name[index[*vp.first]] <<  " ";

    std::cout << std::endl;


    graph_traits<Graph>::edge_iterator ei, ei_end;

    // 辺リストの出力
    std::cout << "edges(g)    = ";

    for (tie(ei, ei_end) = edges(*g); ei != ei_end; ++ei) 
	{

	    std::cout << "(" << name[index[source(*ei, *g)]]
		      << "," << name[index[target(*ei, *g)]] << ")";
	    
	}

    std::cout << std::endl;
    delete g;
    
    return 0;
}
