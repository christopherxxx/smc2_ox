#include <oxexport.h>
#include <fstream>
#include <string>
#include <sstream>
#include <stdint.h>
#include <map>
#include <vector>
#include <stdio.h>
#include <iostream>                      // for std::cout
#include <utility>                       // for std::pair

#include <boost/utility.hpp>             // for boost::tie
#include <boost/graph/graph_traits.hpp>  // for boost::graph_traits
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/copy.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>


using namespace boost;

enum vertex_vt_t
    {
	vertex_vt
    };
enum vertex_vnum_t
    {
	vertex_vnum
    };
enum vertex_vlogw_t
    {
	vertex_vlogw
    };
namespace boost
{
    BOOST_INSTALL_PROPERTY(vertex,vt);
    BOOST_INSTALL_PROPERTY(vertex,vnum);
    BOOST_INSTALL_PROPERTY(vertex,vlogw);
};

typedef property<vertex_vt_t,int,property<vertex_vnum_t,int,property<vertex_vlogw_t,double,property<vertex_index_t,int,property<vertex_color_t,default_color_type> > > > > VertexProperty;
typedef property<edge_weight_t,int> EdgeProperty;
typedef adjacency_list<listS, listS, directedS,VertexProperty,EdgeProperty> Graph;
typedef graph_traits<Graph>::vertex_iterator VI;
typedef graph_traits<Graph>::vertex_descriptor VD;
typedef graph_traits<Graph>::edge_descriptor ED;
typedef graph_traits<Graph>::adjacency_iterator AI;

typedef std::map<int,VD> terminal_nodes;
typedef std::pair<Graph*,terminal_nodes*> Gpair;
typedef std::map<int,Gpair*> address_map;

extern "C" void OXCALL FnInitAllSIR(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnInitSIR(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnAddSIR(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnFreeSIR(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnGraphml(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnCopySIR(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnTransitionSIR(OxVALUE *rtn, OxVALUE *pv, int cArg);

// struct custom_dfs_visitor : public boost::default_dfs_visitor 
// {
//     template <typename VD, typename Graph>
//     void discover_vertex(VD u, const Graph& g) const
//     {
// 	std::cout << u.t << std::endl;
//     }
// };


Gpair* init_sir()
{
    Graph *g = new Graph;
    VD new_node=add_vertex(*g);

    put(vertex_vt_t(),*g,new_node,-1);
    put(vertex_vnum_t(),*g,new_node,0);
    put(vertex_vlogw_t(),*g,new_node,0);
    
    terminal_nodes *tn = new terminal_nodes;
    (*tn)[0]=new_node;
    Gpair *gpair =new Gpair;
    (*gpair).first=g;
    (*gpair).second=tn;
    return gpair;
}

Gpair* copy_sir(Gpair *gpair)
{
    typedef std::map<VD,VD> VDMAP;
    Graph *new_g = new Graph;
    terminal_nodes *new_tn = new terminal_nodes;
    VDMAP m;
    associative_property_map<VDMAP> pm(m);
    Graph* g=(*gpair).first;
    terminal_nodes *tn=(*gpair).second;
    copy_graph(*g,*new_g,orig_to_copy(pm));

    for(terminal_nodes::iterator it=(*tn).begin();it!=(*tn).end();it++)
	{
	    (*new_tn)[(*it).first]=m[(*it).second];
	}
    
    Gpair *new_gpair= new Gpair;
    (*new_gpair).first=new_g;
    (*new_gpair).second=new_tn;
    return new_gpair;
}


void prune(Graph *g, terminal_nodes *target_tn, Graph *new_g,terminal_nodes *new_tn)
{

    VI vi,vi_end;
    
    for (tie(vi, vi_end) = vertices(*g); vi != vi_end; ++vi)
	{
	    put(vertex_color,*g,*vi,white_color);
	}
    
    
    terminal_nodes::iterator it = (*target_tn).begin();
    std::map<VD,VD> vmap;
    
    while( it != (*target_tn).end() )
    	{
    	    bool cond=true;
    	    AI ai,ai_end;
    	    VD tmp_v = (*it).second;
    	    put(vertex_color,*g,tmp_v,black_color);

	    VD new_node=add_vertex(*new_g);
	    (*new_tn)[(*it).first]=new_node;
	    put(vertex_vt_t(),*new_g,new_node,get(vertex_vt_t(),*g,tmp_v));
	    put(vertex_vlogw_t(),*new_g,new_node,get(vertex_vlogw_t(),*g,tmp_v));
	    put(vertex_vnum_t(),*new_g,new_node,get(vertex_vnum_t(),*g,tmp_v));
	    
    	    while(cond)
    		{
    		    tie(ai, ai_end) = adjacent_vertices(tmp_v, *g);
    		    if(ai!=ai_end)
    			{
			    VD old_node=new_node;

			    
			    if(get(vertex_color,*g,*ai)==black_color)
				{
				    add_edge(old_node,vmap[*ai],*new_g);
				    cond=false;
				}
			    else
				{
				    new_node=add_vertex(*new_g);
				    put(vertex_vt_t(),*new_g,new_node,get(vertex_vt_t(),*g,*ai));
				    put(vertex_vlogw_t(),*new_g,new_node,get(vertex_vlogw_t(),*g,*ai));
				    put(vertex_vnum_t(),*new_g,new_node,get(vertex_vnum_t(),*g,*ai));
				    add_edge(old_node,new_node,*new_g);		    

				    put(vertex_color,*g,*ai,black_color);
				    vmap[*ai]=new_node;
				    
				    tmp_v=*ai;
				}

    			}
    		    else
    			{
    			    cond=false;
    			}
    		}
	    
    	    //std::cout << "typeid" <<typeid((*it).second).name() << std::endl;
    	    //boost::dijkstra_shortest_paths(*g, (*it).second,boost::predecessor_map(&parents[0]));
	    
    	    ++it;
    	}
    // VI next;
    // tie(vi,vi_end) = vertices(*g);
    // for (next=vi; vi != vi_end; vi=next)
    // 	{
    // 	    ++next;
    // 	    if(get(vertex_color,*g,*vi)==white_color)
    // 		{
    // 		    clear_vertex(*vi,*g);
    // 		    remove_vertex(*vi,*g);
    // 		}
    // 	}    
}

void OXCALL FnInitAllSIR(OxVALUE *rtn, OxVALUE *pv, int cArg)
{
    address_map *admap = new address_map;
    OxLibValMatMalloc(rtn, 1, 1);
    OxMat(rtn,0)[0][0]=(long)admap;
}


void OXCALL FnInitSIR(OxVALUE *rtn, OxVALUE *pv, int cArg)
{    
    int i, j, r;

    // read variables
    OxLibCheckType(OX_INT, pv, 0, 0);    
    r = OxInt(pv, 0);

    // process
    Gpair* gpair=init_sir();

    address_map* admap=reinterpret_cast<address_map*>(r);
    int index=(*admap).size();
    (*admap)[index]=gpair;
    
    // return
    OxLibValMatMalloc(rtn, 1, 1);
    OxMat(rtn,0)[0][0]=index;
}
void OXCALL FnAddSIR(OxVALUE *rtn, OxVALUE *pv, int cArg)
{    
    int i, j, tree,index,t;
    int nrow,ispruning;
    
    // read variables
    OxLibCheckType(OX_INT, pv, 0, 2);    
    tree = OxInt(pv, 0);
    index = OxInt(pv, 1);
    t = OxInt(pv,2);

    OxLibCheckType(OX_MATRIX,pv,3,3);
    nrow = OxMatr(pv,3);

    OxLibCheckType(OX_INT,pv,4,4);
    ispruning=OxInt(pv,4);
    
    // process
    address_map* admap=(address_map*)((long)tree);    
    Gpair* gpair=(*admap)[index];

    Graph* g=(*gpair).first;
    terminal_nodes* tn=(*gpair).second;

    terminal_nodes* new_tn = new terminal_nodes;
    
    for(i=0;i<nrow;i++)
	{
	    
	    VD new_node = add_vertex(*g);
	    (*new_tn)[i]=new_node;
	    
	    put(vertex_vnum_t(),*g,new_node,i);
	    put(vertex_vt_t(),*g,new_node,t);
	    
	    int source_num=OxMat(pv,3)[i][0];
	    VD source_node=(*tn)[source_num];
	    ED new_edge=add_edge(new_node,source_node,*g).first;	    
	    put(edge_weight_t(),*g,new_edge,1);
	    
	    put(vertex_vlogw_t(),*g,new_node,get(vertex_vlogw_t(),*g,source_node)); //temprorarily copy
	}
    
 
    if(ispruning)
	{
	    // prune unecessary part
	    Graph* output_g = new Graph;
	    terminal_nodes* output_tn = new terminal_nodes;
	    prune(g,new_tn,output_g,output_tn);
	    (*gpair).first=output_g;
	    (*gpair).second=output_tn;
	    delete new_tn;
	    delete tn;
	    delete g;
	}
    else
	{
	    (*gpair).first=g;
	    (*gpair).second=new_tn;
	    delete tn;
	}
    
    // return
    OxLibValMatMalloc(rtn, 1, 1);
    OxMat(rtn,0)[0][0]=1;
}

void OXCALL FnTransitionSIR(OxVALUE *rtn, OxVALUE *pv, int cArg)
{    
    int i, j, tree,index,start,end;
    int nrow,ispruning;
    
    // read variables
    OxLibCheckType(OX_INT, pv, 0, 3);    
    tree = OxInt(pv, 0);
    index = OxInt(pv, 1);
    start = OxInt(pv,2);
    end = OxInt(pv,3); // end is t

    OxLibCheckType(OX_MATRIX,pv,4,4);
    nrow = OxMatr(pv,4);

    // process
    address_map* admap=(address_map*)((long)tree);    
    Gpair* gpair=(*admap)[index];

    Graph* g=(*gpair).first;
    terminal_nodes* tn=(*gpair).second;


    OxLibValMatMalloc(rtn, nrow, end-start+1);
    
    for(i=0;i<nrow;i++)
	{
	    // assign values
	    VD node = (*tn)[i];
	    put(vertex_vnum_t(),*g,node,i);
	    put(vertex_vt_t(),*g,node,end);
	    put(vertex_vlogw_t(),*g,node,get(vertex_vlogw_t(),*g,node)+OxMat(pv,4)[i][0]);
	    
	    // get xi
	    
    	    bool cond=true;
    	    AI ai,ai_end;
    	    VD tmp_v = node;
	    OxMat(rtn,0)[i][end-start]=get(vertex_vlogw_t(),*g,node);

	    int current_position;
	    current_position=end;
	    
    	    while(cond && (current_position>start))
    		{
		    // step forward
		    current_position=current_position-1;
    		    tie(ai, ai_end) = adjacent_vertices(tmp_v, *g);
    		    if(ai!=ai_end)
    			{			    
			    OxMat(rtn,0)[i][get(vertex_vt_t(),*g,*ai)-start]=get(vertex_vlogw_t(),*g,*ai);
			    tmp_v=*ai;
    			}
		    else
    			{
			    std::cout << "warning: missing root" << std::endl;
    			    cond=false;
    			}
    		}

	}
}


// tree_adr, tree_index ,source_nodes
void OXCALL FnCopySIR(OxVALUE *rtn, OxVALUE *pv, int cArg)
{
    int i, j, tree,t;
    int nrow;
    
    // read variables
    OxLibCheckType(OX_INT, pv, 0, 0);    
    tree = OxInt(pv, 0);
    OxLibCheckType(OX_MATRIX,pv,1,2);
    nrow = OxMatr(pv,1);
    
    address_map* admap=(address_map*)((long)tree);
    
    OxLibValMatMalloc(rtn, nrow, 1);    
    //process
    for(i=0;i<nrow;i++)
	{	    
	    int source=(int)OxMat(pv,2)[i][0]; //selected_theta
	    int index=OxMat(pv,1)[source][0];  // tree_index for the selected theta
	    Gpair* gpair=(*admap)[index];
	    int new_index=(*admap).size();
	    (*admap)[new_index]=copy_sir(gpair);
	    OxMat(rtn,0)[i][0]=new_index;
	}    
}
void OXCALL FnFreeSIR(OxVALUE *rtn, OxVALUE *pv, int cArg)
{
    int i, j, tree,t;
    int nrow_trees;
    
    // read variables
    OxLibCheckType(OX_INT, pv, 0, 0);    
    tree = OxInt(pv, 0);
    OxLibCheckType(OX_MATRIX,pv,1,1);
    nrow_trees=OxMatr(pv,1);    

    address_map* admap=(address_map*)((long)tree);
    for(i=0;i<nrow_trees;i++)
	{
	    int index=OxMat(pv,1)[i][0];
	    Gpair* gpair=(*admap)[index];
	    Graph* g=(*gpair).first;
	    terminal_nodes* tn=(*gpair).second;
	    delete tn;
	    delete g;
	    delete gpair;
	}
    OxLibValMatMalloc(rtn, 1, 1);
    OxMat(rtn,0)[0][0]=1;
}


struct my_node_writer 
{

    // my_node_writer() {}
    my_node_writer(Graph& g_) : g (g_) 
    {
    };

    template <class Vertex>
    void operator()(std::ostream& out, Vertex v) 
    {
	out << " [label=\"" << exp(get(vertex_vlogw_t(),g,v)) << "\"]" << std::endl;
    };
    Graph g;

};



void OXCALL FnGraphml(OxVALUE *rtn, OxVALUE *pv, int cArg)
{
    int tree,index;
    OxLibCheckType(OX_INT, pv, 0, 1);    
    tree = OxInt(pv, 0);
    index = OxInt(pv, 1);
    
    address_map* admap=(address_map*)((long)tree);    
    Gpair* gpair=(*admap)[index];
    Graph* g=(*gpair).first;

    std::cout << "nodes:" << num_vertices(*g) << ",edges:" << num_edges(*g) << std::endl;

    VI vi,vi_end;
    int count;
    count=0;

    for (tie(vi, vi_end) = vertices(*g); vi != vi_end; ++vi)
	{
	    // adhoc
	    put(vertex_index,*g,*vi,count);
	    count++;
	}

    std::ofstream file("test.dot");
    boost::write_graphviz(file,*g,my_node_writer(*g));
    
    // boost::dynamic_properties dp;
    // dp.property("color", get(vertex_color, *g));
    // dp.property("node_id", get(vertex_index, *g));
    // write_graphml(file, *g, dp,true);
}

