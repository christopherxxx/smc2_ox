#include<oxstd.h>
#include<src/resampling.ox>

extern "sirtree,FnInitAllSIR" InitAllSIR();
extern "sirtree,FnInitSIR" InitSIR(const tree);
extern "sirtree,FnFreeSIR" FreeSIR(const tree, const index);
extern "sirtree,FnAddSIR" AddSIR(const tree, const index,const t,const mat,const ispruning);
extern "sirtree,FnGraphml" Graphml(const tree, const index);
extern "sirtree,FnCopySIR" CopySIR(const tree, const source, const index);
extern "sirtree,FnTransitionSIR" TransitionSIR(const tree, const index, const from, const to,const logw);

main()
{
    decl i;
    decl N=1;
    decl ti=timer();
    for(i=0;i<N;i++)
	{
	    println("iter: ",i);
	    
	    decl t;
	    decl Nx=300;
	    decl T=500;
	    decl tree=InitAllSIR();
	    println("sirtree address:",tree);
	    decl index=InitSIR(tree);

	    // init
	    decl selected_index=zeros(Nx,1);
	    AddSIR(tree,index,0,selected_index,1);
	    
	    for(t=0;t<T;t++)
		{

		    // transition
		    decl xi=TransitionSIR(tree,index,0,t,ranu(Nx,1));
		    Graphml(tree,index);
		    savemat("test.csv",xi);
		    
		    // resample
		    selected_index=resampling(ranu(Nx,1),Nx);
		    AddSIR(tree,index,t+1,selected_index,imod(t,3));
		    decl new_index=CopySIR(tree,index,0); // for the test of copy sir (meaningless)
		    FreeSIR(tree,index);
		    index=new_index;
		}
	}
    
}

