#ifndef SSM
#define SSM

#include <oxstd.h>
#include <src/wishart.ox>

/*
class State Space Model

Users must define functions specific to a model i.e. virtual function
ex. gen_first_state/gen_observation/transtion/ranprior/densprior/proposal

Other functions are provided in this class
*/
class SSM{
public:
    SSM();
    //model specific functions supposed to be defined
    // creating particles xs given theta rather than single particle x
    virtual gen_first_state(const mparams,const Nx,const Ntheta);
    // creating observations at once as well
    virtual gen_observation(const states,const mparams,const Nx,const Ntheta);
    virtual transition(const states,const obs,const mparams,const t,const Nx,const Ntheta);
    virtual ranprior(const Ntheta);
    virtual densprior(const mparams);

    tree(const tree_adr,const tree,const log_weight,const t,const start);    
    proposal(const mtheta);
    set_proposal(const isdep);
    
    // common variables
    decl dim_obs,dim_state,dim_params;
    decl cat_params;
    decl num_param;
    decl p;     // ex. p[alpha].min/max represents range of alpha matrix

    // calc dimention and set min/max/dim
    init_params();

    // get matrix/vector/scalor from pvec (flatten form of parameters)
    get(const index,const pvec);

    // synthetic data
    virtual synthetic(const params,const t);

    show_paraminfo();
    output_paraminfo(const fname);
    vecp(const param_ary);

    dep_indep_proposal(const mtheta,const index,const isdep,const trunc_index);
    decl isdep;
};
SSM::init_params()
{
    // p[param_index]={name,nrow,ncol,min,max,dim}
    decl i;
    decl total=0;
    for(i=0;i<sizeof(p);i++)
	{
	    decl d=p[i][1]*p[i][2];
	    p[i][3]=total;
	    total += d;
	    p[i][4]=total-1;
	    p[i][5]=d;
	}
    //t println(p);
    dim_params=total;
}

SSM::set_proposal(const dep)
{
    isdep=dep;
}


SSM::show_paraminfo()
{
    println(p);
}

SSM::output_paraminfo(const fname)
{
    decl i,j,k;
    decl file;
    file=fopen(fname,"w");
    fprint(file,"name,row,col\n");
    for(i=0;i<sizeof(p);i++)
	{
	    for(j=0;j<p[i][1];j++)
		{
		    for(k=0;k<p[i][2];k++)
			{
			    fprint(file,p[i][0]);
			    fprint(file,",");
			    fprint(file,j);
			    fprint(file,",");
			    fprint(file,k);
			    fprint(file,"\n");
			}
		}
	}
    fclose(file);
}


// select and reshape data concerned
SSM::get(const index,const pvec){
    decl pinfo=p[index];
    decl nrow=pinfo[1];
    decl ncol=pinfo[2];
    decl pmin=pinfo[3];
    decl pmax=pinfo[4];
    
    decl target=pvec[pmin:pmax];    
    return reshape(target,nrow,ncol);
}




SSM::vecp(const param_ary)
{
    decl i;
    decl body=vecr(param_ary[0]);
    
    for(i=1;i<cat_params;i++)
	{
	    body=body|vecr(param_ary[i]);
	}

    return body;
}

SSM::dep_indep_proposal(const mtheta,const index,const isdep,const trunc_index)
{
    decl Ntheta=rows(mtheta);
    decl i;
    decl dim_index=rows(vec(index));
    decl result=ones(Ntheta,dim_params);
    decl selected_mtheta=mtheta[][index];
    decl logdensdiff=ones(Ntheta,1);

    // calc stat of proposal
    decl varw=2;
    decl meanvec=vec(meanc(selected_mtheta));
    decl medianvec=vec(quantilec(selected_mtheta,ones(dim_index,1)*0.5));
    decl covmat=variance(selected_mtheta).*(varw*unit(dim_index));
    
    decl ch_covmat=choleski(covmat);
    decl inv_ch_covmat=invert(ch_covmat);
    
    for(i=0;i<Ntheta;i++)
	{

	    decl condition=1;
	    while(condition)
		{
		    condition=0;
		    decl original=selected_mtheta[i][]';
		    if(isdep)
			{
			    result[i][index]=(original+ch_covmat*rann(dim_index,1))';
			    logdensdiff[i]=0;
			    
			}
		    else
			{		    
			    decl prop_i=meanvec+ch_covmat*rann(dim_index,1);
			    result[i][index]=prop_i';
			    
			    // ignore Jacobian b/c calc only logdensdiff
			    decl logdens_org=sumc(log(densn(inv_ch_covmat*(original-meanvec))));
			    decl logdens_new=sumc(log(densn(inv_ch_covmat*(prop_i-meanvec))));
			    logdensdiff[i]=logdens_new-logdens_org;
			}

		    //rejection
		    decl j;
		    for(j=0;j<dim_index;j++)
			{
			    if(trunc_index[j]&&(result[i][j]>=1 || result[i][j]<=-1))
				{
				    condition=1;
				}
			}
		}
	}
    
    return {result,logdensdiff};
}

SSM::tree(const tree_adr,const tree,const log_weight,const t,const window_start)
{
    decl Nx=rows(log_weight);
    
    // xi from 0 to t
    // logw can be normalized
    decl transition_log_weight=setbounds(log_weight,-100,100);
    transition_log_weight=transition_log_weight-max(transition_log_weight);		    
    
    decl logwhistory=TransitionSIR(tree_adr,tree,window_start-1,t,transition_log_weight);

    logwhistory=setbounds(logwhistory,-200,200);
    
    decl xi_size=t+2-window_start;
        
    decl xi=(ones(Nx,xi_size)./exp(logwhistory));
    decl xi_scale=ones(1,xi_size)./sumc(xi);
    xi=xi.*reshape(xi_scale,Nx,xi_size).*reshape(Nx,Nx,xi_size);
    
    decl xi_unweighted_sum_weight=(exp(setbounds(log_weight,-100,100)))'*xi; //unweighted f(yt|xt) with respect to different xi
    xi_unweighted_sum_weight=setbounds(xi_unweighted_sum_weight,(-1)*exp(200),exp(200));
    decl equally_weighted_sum_weight=sumc(exp(setbounds(log_weight,-100,100)));
    
    decl log_discount_factor=reshape(log(equally_weighted_sum_weight),1,xi_size)-log(xi_unweighted_sum_weight);
    setbounds(log_discount_factor,-2000,2000);
    
    log_discount_factor[xi_size-1]=0;
    // the end value of the logwhistory is cumulative weight until t
    // the end data is only used for the calc of discount factor
    return {log(xi_unweighted_sum_weight[0:xi_size-2]),log_discount_factor};
}

SSM::proposal(const mtheta)
{
    decl index=range(0,(dim_params-1));        
    return dep_indep_proposal(mtheta,index,isdep);
}


#endif