#include<boost/numeric/ublas/vector.hpp>
#include<boost/numeric/ublas/matrix.hpp>
#include<boost/random.hpp>
#include<boost/tuple/tuple.hpp>
#include<boost/assign.hpp>
#include<ctime>
#include<iostream>
#include<iomanip>

/*
VectorAutoRegression
<state equation>
x_{t+1}=alpha+Phi*x_t+eta
eta~N(0,SigEta)
<observation equation>
y=x+epsilon
epsilon~N(0,SigEps)
<init state>
x_0~N(mu,sig2*I)
<params>
alpha:k*1
Phi: k*k
SigEta: k*k
SigEps: k*k
mu: k*1
sig2: k*1
*/
using namespace boost::numeric::ublas;
typedef vector<double> vec_d;
typedef matrix<double> mat_d;

struct param{
    int nrow;
    int ncol;
    int min;
    int max;
    char name[15];
};


/* 
temporary place
to be moved to src/SSM.hpp
*/
class SSM{
public:
    SSM(){};
    /* model specific functions supposed to be defined */
    // creating particles xs given theta rather than single particle x
    virtual void gen_first_state(mat_d& states,vec_d& params,int size)=0;
    // creating observations at once as well
    virtual void gen_observation(mat_d& state,vec_d& params,int size)=0;
    virtual void transition(mat_d& states,mat_d& obs,vec_d& params,int t)=0;

    /* common variables*/
    int dim_obs,dim_state,dim_params;
    int num_param;
    std::vector<param> p;     // ex. p[alpha].min/max represents range of alpha matrix

    /* common functions*/
    void init_params(){
        num_param=p.size();
        dim_params=index_param_ary();
    };
    // set absolute min and max by i,nrow,ncol
    // calc total dimention
    int index_param_ary(){
        int i=0;
        int total_num=0;
        std::cout << "====== parameter list======" << std::endl;
        for(i=0;i<num_param;i++){
            p[i].min=total_num;
            total_num += p[i].nrow * p[i].ncol;
            p[i].max=total_num-1;
            param a = p[i];
            std::cout << i <<":" << std::setw(15) << a.name << " : " << a.nrow << "*" << a.ncol <<" : from " << std::setw(5) << a.min << " to " << std::setw(5) << a.max << std::endl;
        }
        return total_num;
    };
    
    // synthetic data
    void synthetic(vec_d& params,int size){
        mat_d states(dim_state,1);
        gen_first_state(states,params,1);
    }
  
};


class VAR : public SSM
{
public:
    /*constructor*/
    VAR(int ds){
        dim_state=ds;
        int k =ds;

        /*parameter setting*/
        alpha=0;Phi=1;SigEta=2;SigEps=3;mu=4;sig2=5;
        param palpha = {k,1,0,0,"alpha"};
        param pPhi = {k,k,0,0,"Phi"};
        param pSigEta = {k,k,0,0,"SigEta"};
        param pSigEps = {k,k,0,0,"SigEps"};
        param pmu = {k,1,0,0,"mu"};
        param psig2 = {k,1,0,0,"sig2"};
        using namespace boost::assign;
        p += palpha,pPhi,pSigEta,pSigEps,pmu,psig2;
        init_params();
     };

    ~VAR(){};
      void gen_first_state(mat_d& states , vec_d& params,int size){
      // set mat_mu, mat_sig2
        mat_d mat_mu(p[mu].nrow,p[mu].ncol);
        mat_d mat_sig2(p[sig2].nrow,p[sig2].ncol);
        std::copy(params.begin()+p[mu].min,params.begin()+p[mu].max,mat_mu.begin1());
        std::copy(params.begin()+p[sig2].min,params.begin()+p[sig2].max,mat_sig2.begin1());
	
      };
    void gen_observation(mat_d& states,vec_d& params, int size){
    };
    void transition(mat_d& states,mat_d& obs,vec_d& params,int t){
	
    };
    
    int alpha,Phi,SigEta,SigEps,mu,sig2;
};

