#include <oxstd.h>
#include <model/SSM.ox>
#include <src/sigmoid.ox>
/*
VectorAutoRegression
<state equation>
x_{t+1}=alpha+Phi*x_t+eta
eta~N(0,SigEta)
<observation equation>
y=x+epsilon
epsilon~N(0,SigEps)
<init state>
x_0~N(mu,sig2*I)
<params>
alpha:k*1
Phi: k*k
SigEtaL: k*k Lower Triangle Matrix whose digonal vector is 1
SigEtaD: k*1 Diagnoal Vector
SigEpsL: k*k Lower Triangle Matrix whose digonal vector is 1
SigEtaD: k*1 Diagnoal Vector
mu: k*1
sig2: k*1
*/

class VAR : public SSM
{
public:    
    VAR(const ds);
    ~VAR();
    gen_first_state(const mparams,const Nx,const Ntheta);
    gen_observation(const states,const mparams,const Nx,const Ntheta);
    transition(const states,const obs,const mparams,const t,const Nx,const Ntheta);
    densprior(const mparams);
    ranprior(const Ntheta);
    proposal(const mparams);
    synthetic(const params,const t);
    get_lower(const index,const pvec);
    decl alpha,Phi,SigEtaL,logSigEtaD,SigEpsL,logSigEpsD,mu,logsig2;
};

VAR::VAR(const ds)
{
    dim_state=ds;
    dim_obs=ds;
    // set param configuration
    alpha=0;Phi=1;SigEtaL=2;logSigEtaD=3;SigEpsL=4;logSigEpsD=5;mu=6;logsig2=7;
    p={{"alpha",ds,1,0,0,0},{"Phi",ds,ds,0,0,0},{"SigEtaL",ds,ds,0,0,0},{"logSigEtaD",ds,1,0,0,0},{"SigEpsL",ds,ds,0,0,0},{"logSigEpsD",ds,1,0,0,0},{"mu",ds,1,0,0,0},{"logsig2",ds,1,0,0,0}};
    init_params();
    cat_params=sizeof(p);
}


VAR::proposal(const mtheta)
{
    //decl index=range(0,(dim_params-1));
    decl index=<0,1,2,3,4,5,8,10,11,14,16,17,18,19,20,21>;
    return dep_indep_proposal(mtheta,index,1);
}


VAR::ranprior(const Ntheta)
{
    decl result=zeros(Ntheta,dim_params);
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    decl tmp_var=2;
	    decl sm_var=1.7;
	    decl phi_var=1;
	    decl palpha=tmp_var*rann(p[alpha][1],1);
	    decl pPhi=phi_var*rann(p[Phi][1],p[Phi][1]);
	    decl pSigEtaL=sm_var*rann(p[SigEtaL][1],p[SigEtaL][1]);
	    decl plogSigEtaD=rann(p[logSigEtaD][1],1);
	    decl pSigEpsL=sm_var*rann(p[SigEpsL][1],p[SigEpsL][1]);
	    decl plogSigEpsD=rann(p[logSigEpsD][1],1);
	    decl pmu=(rann(p[mu][1],1));
	    decl plogsig2=rann(p[logsig2][1],1);
	    result[i][]=vecp({palpha,pPhi,pSigEtaL,plogSigEtaD,pSigEpsL,plogSigEpsD,pmu,plogsig2})';
	}
    return result;
}

VAR::densprior(const mparams)
{
    // temporarilly return flat prior
    // prior density is only used for PMCMC step especcially in MH algorith
    // density should be log form
    decl Ntheta=rows(mparams);
    decl result=zeros(Ntheta,1);
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    decl r=mparams[i][];
	    decl tmp_var=2;
	    decl sm_var=1.7;
	    decl phi_var=1;
	    decl palpha=prodc(densn(get(alpha,r)/tmp_var)/sqrt(tmp_var));
	    decl pPhi=prodr(prodc(densn(get(Phi,r)/phi_var)/sqrt(phi_var)));
	    decl pSigEtaL=prodr(prodc(setupper(lower(densn(get(SigEtaL,r)/sm_var)/sqrt(sm_var)),ones(p[SigEtaL][1],p[SigEtaL][1]),1)));
	    decl plogSigEtaD=prodc(densn(get(logSigEtaD,r)));
	    decl pSigEpsL=prodr(prodc(setupper(lower(densn(get(SigEpsL,r)/sm_var)/sqrt(sm_var)),ones(p[SigEpsL][1],p[SigEpsL][1]),1)));
	    decl plogSigEpsD=prodc(densn(get(logSigEpsD,r)));
	    decl pmu=prodc(densn(get(mu,r)));	    
	    decl plogsig2=prodc(densn(get(logsig2,r)));	    
	    result[i]=log(palpha)+log(pPhi)+log(pSigEtaL)+log(plogSigEtaD)+log(pSigEpsL)+log(plogSigEpsD)+log(pmu)+log(plogsig2);
	}
    result=setbounds(result,-2000,2000);
    //result=zeros(Ntheta,1);
    return result;
}


VAR::synthetic(const params,const t)
{
    decl state=zeros(t+1,dim_state);
    decl obs=zeros(t,dim_state);
    decl mparams=reshape(params,1,dim_params);
    println(mparams);
    
    // sample size is one    
    state[0][]=gen_first_state(mparams,1,1);
    decl i=0;
    for(i=0;i<t;i++)
	{
	    state[i+1][]=transition(state[i][],zeros(dim_state,1),mparams,t,1,1)[0];
	    obs[i][]=gen_observation(state[i+1][],mparams,1,1);
	}
    return {state,obs};
}

// mparams is supposed to be a Ntheta*dim_params  matrix
// states is supposed to be a Ntheta*(Nx*dim_state) matrix
VAR::gen_first_state(const mparams,const Nx,const Ntheta)
{
    decl i;
    decl states=zeros(Ntheta,Nx*dim_state);
    for(i=0;i<Ntheta;i++)
	{
	    // get original parameter in size of dim_state*1
	    // and extend Nx times by reshape
	    decl mmu=reshape(get(mu,mparams[i][]),dim_state*Nx,1);
	    decl msig2=reshape(exp(get(logsig2,mparams[i][])),dim_state*Nx,1);
	    // 1*(Nx*dim_state) vector
	    states[i][]=(mmu+msig2.*rann(dim_state*Nx,1))';
	}
    return states;
}

VAR::gen_observation(const states,const mparams,const Nx,const Ntheta)
{
    decl result=zeros(Ntheta,Nx*dim_obs);
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    decl r=mparams[i][];
	    decl mSigEpsL=get_lower(SigEpsL,r);
	    decl mSigEpsD=exp(get(logSigEpsD,r));	    
	    
	    // temporarily allocate dim_obs*Nx matrix
	    // to create a 1*(Nx*dim_obs) vector
	    decl noise=mSigEpsL*diag(mSigEpsD)*rann(dim_obs,Nx);
	    
	    // extend state[i][] by Nx times
	    // flatten noise in the column-oriented way
	    result[i][]=(reshape(states[i][],Nx*dim_obs,1)+vec(noise))';
	}
    return result;
}

// Note that returned w must be in log form
VAR::transition(const states,const obs,const mparams,const t,const Nx,const Ntheta)
{
 
    decl rstates=zeros(Ntheta,Nx*dim_state);
    decl rweights=zeros(Ntheta,Nx);
    decl i;    
    
    for(i=0;i<Ntheta;i++)
    	{	    
    	    decl r=mparams[i][];
	    decl malpha=get(alpha,r);
    	    decl mPhi=get(Phi,r);
    	    decl mSigEtaL=get_lower(SigEtaL,r);
    	    decl mSigEtaD=exp(get(logSigEtaD,r));
    	    decl mSigEpsL=get_lower(SigEpsL,r);
    	    decl mSigEpsD=exp(get(logSigEpsD,r));
	    
    	    // noise is dim_state*Nx matrix
    	    // supposed to be decomposed in the column-oriented way
    	    decl noise=mSigEtaL*diag(mSigEtaD)*rann(dim_state,Nx);
    	    decl body=mPhi*reshape(states[i][],dim_state,Nx);
    	    // new_state is 1*(Nx*dim_state) vector
    	    decl new_state=(reshape(malpha,Nx*dim_state,1)+vec(body)+vec(noise))';
	    
    	    rstates[i][]=new_state;
    	    // dim_obs*Nx matrix
    	    // each elemtns reperesents likelihood of noise in form of scalar

    	    decl cov_half=mSigEpsL*diag(mSigEpsD);	    
    	    decl invcov_half=invert(cov_half);
    	    decl dif=reshape(vec(new_state)-reshape(obs,dim_obs*Nx,1),Nx,dim_obs)';
	    
    	    decl likelihood=densn(invcov_half*dif)./sqrt(determinant(cov_half*cov_half));
    	    rweights[i][]=sumc(log(likelihood));
    	}
    return {rstates,rweights};
}

VAR::get_lower(const index,const pvec)
{
    decl lower_dat=lower(sigmoid(get(index,pvec)));
    decl result=ones(dim_state,dim_state);
    result=setlower(result,lower_dat);
    result=setupper(result,zeros(dim_state,dim_state));
    
    return result;
    
}
