#include <oxstd.h>
#include <model/SSM.ox>
#include <src/sigmoid.ox>

/*
t-distributed Stochastic volatility model

<observation equation>
y=sqrt(exp(x))*epsilon
epsilon~T(df)

<state equation>
x_t = {h_t,eta_t}
h_{t+1}=mu+phi*(h_t - mu)+eta
eta~N(0,sigeta^2)

<init>
x_0 ~ N(mu,sigeta^2/(1-phi^2))

<params>
mu:1*1
smphi:1*1
logsigeta:1*1
logdf:1*1
*/

class TSV : public SSM
{
public:    
    TSV();
    ~TSV();
    gen_first_state(const mparams,const Nx,const Ntheta);
    gen_observation(const states,const mparams,const Nx,const Ntheta);
    transition(const states,const obs,const mparams,const t,const Nx,const Ntheta,const tree_adr,const trees,const istree,const start);
    densprior(const mparams);
    ranprior(const Ntheta);
    synthetic(const params,const t);
    decl mu,phi,logsigeta,logdf;
    proposal(const mtheta)
};

TSV::TSV()
{
    dim_state=2;
    dim_obs=1;
    // set param configuration
    mu=0;phi=1;logsigeta=2;logdf=3;
    p={{"mu",1,1,0,0,0},{"phi",1,1,0,0,0},{"log_sigeta",1,1,0,0,0},{"log_df",1,1,0,0,0}};
    init_params();
    cat_params=sizeof(p);
}




TSV::ranprior(const Ntheta)
{
    decl result=zeros(Ntheta,dim_params);
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    decl tmp_sd=10;
	    decl df_sd=1;
	    decl pmu=tmp_sd*rann(1,1);
	    decl pphi=ranu(1,1)*2-1;
	    decl plogsigeta=tmp_sd*rann(1,1);
	    decl plogdf=df_sd*(rann(1,1)+2);
	    
	    result[i][]=vecp({pmu,pphi,plogsigeta,plogdf})';
	}
    return result;
}

TSV::densprior(const mparams)
{
    // temporarilly return flat prior
    // prior density is only used for PMCMC step especcially in MH algorith
    // density should be log form
    decl Ntheta=rows(mparams);
    decl result=zeros(Ntheta,1);
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    decl r=mparams[i][];
	    decl tmp_sd=10;
	    decl df_sd=1;
	    decl pmu=prodc(densn(get(mu,r)/tmp_sd)/tmp_sd);
	    decl plogsigeta=prodc(densn(get(logsigeta,r)/tmp_sd)/tmp_sd);
	    decl plogdf=prodc(densn(get(logdf,r)/df_sd-2)/df_sd);
	    
	    result[i]=log(pmu)+log(plogsigeta)+log(plogdf);
	}
    result=setbounds(result,-2000,2000);
    return result;
}


TSV::synthetic(const params,const t)
{
    decl state=zeros(t+1,dim_state);
    decl obs=zeros(t,dim_obs);
    decl mparams=reshape(params,1,dim_params);
    println(mparams);
    
    // sample size is one    
    state[0][]=gen_first_state(mparams,1,1);
    decl i=0;
    for(i=0;i<t;i++)
	{
	    state[i+1][]=transition(state[i][],zeros(dim_state,1),mparams,t,1,1,0,0,0,0)[0];
	    obs[i][]=gen_observation(state[i+1][],mparams,1,1);
	}
    return {state,obs};
}

// mparams is supposed to be a Ntheta*dim_params  matrix
// states is supposed to be a Ntheta*(Nx*dim_state) matrix
TSV::gen_first_state(const mparams,const Nx,const Ntheta)
{
    decl i;
    decl states=zeros(Ntheta,Nx*dim_state);
    for(i=0;i<Ntheta;i++)
	{
	    // get original parameter in size of dim_state*1
	    // and extend Nx times by reshape
	    decl r=mparams[i][];
	    decl mmu=get(mu,r);
	    decl mphi=get(phi,r);
	    decl msigeta=exp(get(logsigeta,r));
	    // 1*(Nx*dim_state) vector
	    
	    decl h=msigeta*(1/sqrt(1-(mphi*mphi)))*rann(Nx,1)+reshape(mmu,Nx,1);
	    decl eta=msigeta*rann(Nx,1);
	    decl state=reshape(h~eta,Nx*2,1);
	    states[i][]=state';	    
	}
    return states;
}

TSV::gen_observation(const states,const mparams,const Nx,const Ntheta)
{
    decl result=zeros(Ntheta,Nx*dim_obs);
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    decl r=mparams[i][];
	    decl msigeta=exp(get(logsigeta,r));
	    decl mdf=exp(get(logdf,r));
	    
	    // temporarily allocate dim_obs*Nx matrix
	    // to create a 1*(Nx*dim_obs) vector
	    decl state_h=reshape(states[i][],Nx,2)[][0];
	    decl state_eta=reshape(states[i][],Nx,2)[][1];
	    
	    decl noise=	exp(state_h/2).*rant(Nx,1,mdf);
	    
	    // extend state[i][] by Nx times
	    // flatten noise in the column-oriented way
	    result[i][]=(noise)';
	}
    return result;
}

// Note that returned w must be in log form
TSV::transition(const states,const obs,const mparams,const t,const Nx,const Ntheta,const tree_adr,const trees,const istree,const start)
{
    decl rstates=zeros(Ntheta,Nx*dim_state);
    decl rweights=zeros(Ntheta,Nx);
    decl i;    
    decl r_log_xi_weight=zeros(Ntheta,t+1-start);
    decl rlogdf=zeros(Ntheta,t+2-start);

    //create h_{t+1} using h_{t} and "eta_{t}"
    // x_t=<h_t,eta_{t-1}>
    for(i=0;i<Ntheta;i++)
    	{	    
    	    decl r=mparams[i][];
	    decl mmu=get(mu,r);
	    decl mphi=get(phi,r);
	    decl msigeta=exp(get(logsigeta,r));
	    decl mdf=exp(get(logdf,r));
	    
    	    // noise is dim_state*Nx matrix
    	    // supposed to be decomposed in the column-oriented way

	    decl old_state=reshape(states[i][],Nx,2);
	    decl old_state_h=old_state[][0];
	    decl old_state_eta=old_state[][1];
	    
	    decl h=reshape(mmu*(1-mphi),Nx,1)+ mphi*old_state_h +old_state_eta;
	    
    	    // new_state is 1*(Nx*dim_state) vector
	    decl eta=msigeta*rann(Nx,1); //create eta, but it is not used
    	    decl new_state=reshape((h~eta),Nx*2,1);
	    
    	    rstates[i][]=new_state';
	    // dim_obs*Nx matrix
    	    // each elemtns reperesents likelihood of noise in form of scalar
	    
	    decl exph2=setbounds(exp(h/2),exp(-200),.Inf);
	    
    	    decl dif=(reshape(obs[0],Nx,1)./exph2);
	    dif=setbounds(dif,-exp(200),exp(200));
	    decl denst_dif=setbounds(denst(dif,mdf),-exp(200),exp(200));	    
    	    decl log_likelihood_without_jaco=setbounds(log(denst_dif),-exp(200),exp(200));

	    decl log_weight=(log_likelihood_without_jaco-log(exph2));

	    if(istree)
		{
		    decl result_xi=tree(tree_adr,trees[i],log_weight,t,start);
		    
		    // the end value of the logwhistory is cumulative weight until t
		    // the end data is only used for the calc of discount factor
		    r_log_xi_weight[i][]=result_xi[0]; 
		    rlogdf[i][]=result_xi[1];
		}
	    
	    rweights[i][]=log_weight'; // weight for resampilng
	    
	    //println(new_state');
	    //println("states",states[i][]');
	    //println(old_state');
	    //println("h",h');
	    //println("dif",dif');
	    //println("yt/exp(h/2)",obs[0]./exp(h/2));

	    //println("like",likelihood);
	    //println(exp(h/2)~likelihood);
    	}

    return {rstates,rweights,r_log_xi_weight,rlogdf};
}

//override
TSV::proposal(const mtheta)
{
    decl index=range(0,(dim_params-1));
    // kernel must be indep
    return dep_indep_proposal(mtheta,index,0,<0,1,0,0>);
}