#include<oxstd.h>
#include<model/VAR.ox>
#include<oxdraw.h>

main()
{
    decl dim=2;
    decl v=new VAR(dim);
    decl alpha=<0,0>;
    decl Phi=<0.99,0.00;0.00,0.99>;
    decl SigEtaL=0.1*ranu(dim,dim);
    decl SigEtaD=ranu(dim,1);
    decl SigEpsL=0.1*ranu(dim,dim);
    decl SigEpsD=ranu(dim,1);
    decl mu=zeros(dim,1);
    decl sig2=ones(dim,1);
decl params=vec(alpha)|vec(Phi)|vec(SigEtaL)|vec(SigEtaD)|vec(SigEpsL)|vec(SigEpsD)|vec(mu)|vec(sig2);
    // T=10**5 takes several seconds
    decl result=v.synthetic(params,100);
    decl obs=result[1];
    decl hidden=result[0];
    decl dat=hidden[1:][]~obs;
    
    DrawMatrix(0,dat',{"h1","h2","o1","o2"},1,1);
    SaveDrawWindow("graph1.eps");
    DrawTitle(0,"Test");
    CloseDrawWindow( );
    println(v.dim_obs);
    v.show_paraminfo();
    println("end");
}

