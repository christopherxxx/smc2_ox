
Ox Console version 7.00 (Linux) (C) J.A. Doornik, 1994-2013
This version may be used for academic research and teaching only
HOMEmodSP500SV_Nx300Ntheta1000Sim0T1983W1000Dep1
SV with leverage

      -6.8482
emp
0 th observation is being processed

      -5.9312   -0.0054367      -6.1669    -0.058269
ess_rate:0.131248
PMCMC
0
a-rate:
      0.35900
1 th observation is being processed

      -8.0027     0.036535      -7.1969    -0.099388
ess_rate:0.586794
2 th observation is being processed

      -8.8894      0.13895      -5.2396     -0.11623
ess_rate:0.241256
PMCMC
0
a-rate:
      0.24900
3 th observation is being processed

      -8.9174     0.077817      -7.5322    -0.072134
ess_rate:0.824062
4 th observation is being processed

      -9.2447     0.085234      -7.8817    -0.070629
ess_rate:0.687644
5 th observation is being processed

      -9.4880      0.13623      -6.3606     -0.13152
ess_rate:0.628039
6 th observation is being processed

      -9.6587      0.14702      -6.5450     -0.12782
ess_rate:0.489937
7 th observation is being processed

      -9.7275      0.10473      -7.5600    -0.083828
ess_rate:0.397774
PMCMC
0
a-rate:
      0.34000
8 th observation is being processed

      -9.8857      0.10420      -7.1648    -0.065289
ess_rate:0.921194
9 th observation is being processed

      -9.9624     0.099697      -7.2138    -0.071667
ess_rate:0.845094
10 th observation is being processed

      -10.018     0.093943      -7.3622    -0.068652
ess_rate:0.759798
11 th observation is being processed

      -10.042     0.083266      -7.5790    -0.070106
ess_rate:0.707161
12 th observation is being processed

      -9.6749      0.15617      -7.4864    -0.030804
ess_rate:0.737262
13 th observation is being processed

      -9.7522      0.10787      -7.4516     0.012527
ess_rate:0.52688
14 th observation is being processed

      -9.8197      0.12205      -7.4679    -0.014203
ess_rate:0.733705
15 th observation is being processed

      -9.8916      0.11544      -6.8820    -0.014419
ess_rate:0.630707
16 th observation is being processed

      -9.8684      0.10032      -7.3916    -0.016658
ess_rate:0.652204
17 th observation is being processed

      -9.8577     0.091700      -7.7945    -0.012334
ess_rate:0.652212
18 th observation is being processed

      -9.9105     0.099259      -7.3320    -0.036663
ess_rate:0.677793
19 th observation is being processed

      -9.9396     0.096502      -7.1769    -0.042153
ess_rate:0.652547
20 th observation is being processed

      -9.9906     0.099024      -6.7592    -0.039680
ess_rate:0.598857
21 th observation is being processed

      -9.9493     0.080793      -7.3400    -0.028959
ess_rate:0.615296
22 th observation is being processed

      -9.9544     0.073369      -7.5522    -0.029527
ess_rate:0.615189
23 th observation is being processed

      -10.001     0.063447      -7.3241    -0.015637
ess_rate:0.565558
24 th observation is being processed

      -9.9815     0.052946      -7.6962    -0.020589
ess_rate:0.570068
25 th observation is being processed

      -9.9565     0.051568      -7.9938    -0.033944
ess_rate:0.57552
26 th observation is being processed

      -9.9924     0.051072      -7.8091    -0.048855
ess_rate:0.566281
27 th observation is being processed

      -10.019     0.054482      -7.6776    -0.045571
ess_rate:0.541899
28 th observation is being processed

      -10.043     0.055470      -7.5341    -0.050459
ess_rate:0.523574
29 th observation is being processed

      -10.005     0.040834      -7.8347    -0.058208
ess_rate:0.534698
30 th observation is being processed

      -10.028     0.035878      -7.7438    -0.065926
ess_rate:0.519738
31 th observation is being processed

      -10.023     0.023714      -7.9718    -0.061821
ess_rate:0.503292
32 th observation is being processed

      -10.051     0.019466      -7.6672    -0.080806
ess_rate:0.482815
33 th observation is being processed

      -10.067     0.027868      -7.6613    -0.072530
ess_rate:0.475333
34 th observation is being processed

      -10.053     0.031098      -7.9193    -0.066209
ess_rate:0.468865
35 th observation is being processed

      -10.071     0.028382      -7.8936    -0.068951
ess_rate:0.45032
36 th observation is being processed

      -10.096     0.027383      -7.8179    -0.064524
ess_rate:0.422206
37 th observation is being processed

      -10.114     0.025306      -7.7988    -0.063776
ess_rate:0.399846
PMCMC
0
a-rate:
      0.48900
38 th observation is being processed

      -10.088     0.024008      -8.3863    -0.037798
ess_rate:0.951357
39 th observation is being processed

      -10.072     0.027847      -8.4777    -0.044128
ess_rate:0.919406
40 th observation is being processed

      -10.097     0.021241      -8.4506    -0.051484
ess_rate:0.955366
41 th observation is being processed

      -10.120     0.020579      -8.4519    -0.048097
ess_rate:0.974737
42 th observation is being processed

      -10.114     0.021030      -8.5553    -0.046123
ess_rate:0.95919
43 th observation is being processed

      -10.136     0.015543      -8.6334    -0.037282
ess_rate:0.960462
44 th observation is being processed

      -10.158    0.0098065      -8.6482    -0.034338
ess_rate:0.949799
45 th observation is being processed

      -10.167    0.0075297      -8.6987    -0.033528
ess_rate:0.934375
46 th observation is being processed

      -10.156    0.0064921      -8.7527    -0.036527
ess_rate:0.927229
47 th observation is being processed

      -10.176    0.0011790      -8.7391    -0.038365
ess_rate:0.907629
48 th observation is being processed

      -10.142    0.0039032      -8.7616    -0.042375
ess_rate:0.90187
49 th observation is being processed

      -10.152    0.0022831      -8.8000    -0.043612
ess_rate:0.886566
50 th observation is being processed

      -10.169   0.00035459      -8.8005    -0.043986
ess_rate:0.879047
51 th observation is being processed

      -10.188   -0.0014002      -8.7589    -0.043427
ess_rate:0.873225
52 th observation is being processed

      -10.205   -0.0024309      -8.7245    -0.043907
ess_rate:0.845881
53 th observation is being processed

      -10.205   -0.0067898      -8.8133    -0.043627
ess_rate:0.820503
54 th observation is being processed

      -10.207   -0.0085556      -8.8878    -0.040014
ess_rate:0.810186
55 th observation is being processed

      -10.224    -0.013441      -8.8999    -0.037815
ess_rate:0.789641
56 th observation is being processed

      -10.243    -0.016044      -8.9276    -0.027810
ess_rate:0.751212
57 th observation is being processed

      -10.261    -0.022700      -8.8777    -0.026806
ess_rate:0.70711
58 th observation is being processed

      -10.259    -0.025187      -8.9639    -0.025123
ess_rate:0.704121
59 th observation is being processed

      -10.248    -0.022702      -9.0150    -0.028110
ess_rate:0.713591
60 th observation is being processed

      -10.265    -0.029196      -8.9994    -0.028305
ess_rate:0.673602
61 th observation is being processed

      -10.275    -0.031808      -9.0548    -0.023195
ess_rate:0.646656
62 th observation is being processed

      -10.285    -0.037383      -9.1014    -0.018758
ess_rate:0.617689
63 th observation is being processed

      -10.284    -0.036030      -9.1644    -0.016792
ess_rate:0.61538
64 th observation is being processed

      -10.292    -0.039281      -9.1879    -0.015142
ess_rate:0.594362
65 th observation is being processed

      -10.306    -0.043441      -9.1777    -0.014330
ess_rate:0.557444
66 th observation is being processed

      -10.273    -0.034698      -9.1419    -0.024351
ess_rate:0.629992
67 th observation is being processed

      -10.289    -0.043012      -9.1963    -0.012682
ess_rate:0.58887
68 th observation is being processed

      -10.276    -0.041465      -9.2306    -0.017517
ess_rate:0.594016
69 th observation is being processed

      -10.291    -0.048980      -9.2645    -0.010413
ess_rate:0.557984
70 th observation is being processed

      -10.306    -0.053551      -9.2593   -0.0048818
ess_rate:0.532412
71 th observation is being processed

      -10.316    -0.058880      -9.2554   -0.0045518
ess_rate:0.503586
72 th observation is being processed

      -10.219   -0.0084126      -8.7288    -0.048551
ess_rate:0.654112
73 th observation is being processed

      -10.237    -0.022048      -8.5976    -0.055822
ess_rate:0.607242
74 th observation is being processed

      -10.252    -0.022301      -8.5703    -0.051934
ess_rate:0.576604
75 th observation is being processed

      -10.269    -0.032031      -8.3921    -0.056795
ess_rate:0.512211
76 th observation is being processed

      -10.282    -0.031972      -8.3458    -0.054080
ess_rate:0.472945
77 th observation is being processed

      -10.288    -0.036177      -8.3936    -0.050672
ess_rate:0.463688
78 th observation is being processed

      -10.298    -0.040909      -8.4464    -0.039492
ess_rate:0.456349
79 th observation is being processed

      -10.308    -0.046890      -8.4172    -0.038142
ess_rate:0.436582
80 th observation is being processed

      -10.324    -0.052668      -8.2334    -0.041166
ess_rate:0.367245
PMCMC
