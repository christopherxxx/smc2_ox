
Ox Console version 7.00 (Linux) (C) J.A. Doornik, 1994-2013
This version may be used for academic research and teaching only
sampleSIMtestSV_Nx200Ntheta1000Sim0T100W100Dep0
SV with leverage
emp
0 th observation is being processed
ess_rate:0.112354
PMCMC
0
a-rate:
      0.45900
1 th observation is being processed
ess_rate:0.373108
PMCMC
0
a-rate:
      0.51400
2 th observation is being processed
ess_rate:0.864833
3 th observation is being processed
ess_rate:0.690028
4 th observation is being processed
ess_rate:0.666275
5 th observation is being processed
ess_rate:0.584601
6 th observation is being processed
ess_rate:0.505082
7 th observation is being processed
ess_rate:0.413135
8 th observation is being processed
ess_rate:0.332648
PMCMC
0
a-rate:
      0.54000
9 th observation is being processed
ess_rate:0.952673
10 th observation is being processed
ess_rate:0.755591
11 th observation is being processed
ess_rate:0.838213
12 th observation is being processed
ess_rate:0.679413
13 th observation is being processed
ess_rate:0.261107
PMCMC
0
a-rate:
      0.55000
14 th observation is being processed
ess_rate:0.971277
15 th observation is being processed
ess_rate:0.965281
16 th observation is being processed
ess_rate:0.926885
17 th observation is being processed
ess_rate:0.860235
18 th observation is being processed
ess_rate:0.804187
19 th observation is being processed
ess_rate:0.878245
20 th observation is being processed
ess_rate:0.830279
21 th observation is being processed
ess_rate:0.76209
22 th observation is being processed
ess_rate:0.694404
23 th observation is being processed
ess_rate:0.671728
24 th observation is being processed
ess_rate:0.600523
25 th observation is being processed
ess_rate:0.579197
26 th observation is being processed
ess_rate:0.514679
27 th observation is being processed
ess_rate:0.442368
28 th observation is being processed
ess_rate:0.364848
PMCMC
0
a-rate:
      0.45700
29 th observation is being processed
ess_rate:0.988716
30 th observation is being processed
ess_rate:0.965854
31 th observation is being processed
ess_rate:0.887535
32 th observation is being processed
ess_rate:0.902018
33 th observation is being processed
ess_rate:0.843876
34 th observation is being processed
ess_rate:0.710795
35 th observation is being processed
ess_rate:0.652388
36 th observation is being processed
ess_rate:0.447054
37 th observation is being processed
ess_rate:0.294058
PMCMC
0
a-rate:
      0.14700
38 th observation is being processed
ess_rate:0.972823
39 th observation is being processed
ess_rate:0.946354
40 th observation is being processed
ess_rate:0.959699
41 th observation is being processed
ess_rate:0.927179
42 th observation is being processed
ess_rate:0.869841
43 th observation is being processed
ess_rate:0.807052
44 th observation is being processed
ess_rate:0.837034
45 th observation is being processed
ess_rate:0.854175
46 th observation is being processed
ess_rate:0.78755
47 th observation is being processed
ess_rate:0.726712
48 th observation is being processed
ess_rate:0.680279
49 th observation is being processed
ess_rate:0.736381
50 th observation is being processed
ess_rate:0.671454
51 th observation is being processed
ess_rate:0.607133
52 th observation is being processed
ess_rate:0.526123
53 th observation is being processed
ess_rate:0.489476
54 th observation is being processed
ess_rate:0.50183
55 th observation is being processed
ess_rate:0.46744
56 th observation is being processed
ess_rate:0.477944
57 th observation is being processed
ess_rate:0.479277
58 th observation is being processed
ess_rate:0.461407
59 th observation is being processed
ess_rate:0.415532
60 th observation is being processed
ess_rate:0.442034
61 th observation is being processed
ess_rate:0.421407
62 th observation is being processed
ess_rate:0.388682
PMCMC
0
a-rate:
      0.23500
63 th observation is being processed
ess_rate:0.970522
64 th observation is being processed
ess_rate:0.951771
65 th observation is being processed
ess_rate:0.922675
66 th observation is being processed
ess_rate:0.595831
67 th observation is being processed
ess_rate:0.664551
68 th observation is being processed
ess_rate:0.646117
69 th observation is being processed
ess_rate:0.655591
70 th observation is being processed
ess_rate:0.544317
71 th observation is being processed
ess_rate:0.55083
72 th observation is being processed
ess_rate:0.587286
73 th observation is being processed
ess_rate:0.611921
74 th observation is being processed
ess_rate:0.672471
75 th observation is being processed
ess_rate:0.712328
76 th observation is being processed
ess_rate:0.64655
77 th observation is being processed
ess_rate:0.699787
78 th observation is being processed
ess_rate:0.734296
79 th observation is being processed
ess_rate:0.748817
80 th observation is being processed
ess_rate:0.74648
81 th observation is being processed
ess_rate:0.695859
82 th observation is being processed
ess_rate:0.758119
83 th observation is being processed
ess_rate:0.733954
84 th observation is being processed
ess_rate:0.721605
85 th observation is being processed
ess_rate:0.662577
86 th observation is being processed
ess_rate:0.670054
87 th observation is being processed
ess_rate:0.64419
88 th observation is being processed
ess_rate:0.643321
89 th observation is being processed
ess_rate:0.672975
90 th observation is being processed
ess_rate:0.655888
91 th observation is being processed
ess_rate:0.66733
92 th observation is being processed
ess_rate:0.687517
93 th observation is being processed
ess_rate:0.700768
94 th observation is being processed
ess_rate:0.699565
95 th observation is being processed
ess_rate:0.633458
96 th observation is being processed
ess_rate:0.659278
97 th observation is being processed
ess_rate:0.627635
98 th observation is being processed
ess_rate:0.589137
99 th observation is being processed
ess_rate:0.481191
PMCMC
0
a-rate:
      0.21900
end of the program
