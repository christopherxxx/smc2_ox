topix=read.csv("topix_all.csv",header=T)
topix$year=as.numeric(substr(topix[[1]],1,4))
t1=topix[2:nrow(topix),2]
t0=topix[1:nrow(topix)-1,2]
y=(log(t1)-log(t0))
topix$return=c(NA,y)
write.csv(y,"topix_all_return.csv")
topix_2001_2012=topix[topix$year%in%(2001:2012),]
topix_2012_2013=topix[topix$year%in%(2012:2013),]
write.csv(topix_2001_2012$return,"topix_2001_2012_daily_2947.csv")
write.csv(topix_2012_2013$return,"topix_2012_2013_daily_485.csv")

square_sum=function(x){
  x%*%x
}

topix_2001_2012=topix_2001_2012[1:(floor(nrow(topix_2001_2012)/5)*5),]
topix_2001_2012$by5index=floor(0:(nrow(topix_2001_2012)-1)/5)

topixby5sum=aggregate(topix_2001_2012[,c("return")],by=list(topix_2001_2012$by5index),FUN=sum)
topixby5sd=aggregate(topix_2001_2012[,c("return")],by=list(topix_2001_2012$by5index),FUN=square_sum)

topix_2001_2012_by5=data.frame(list(sum=topixby5sum[[2]],sd=topixby5sd[[2]]))
write.csv(topix_2001_2012_by5,"topix_2001_2012_by5_589.csv")


topix_1998_2012=topix[topix$year%in%(1998:2012),]
topix_1998_2012=topix_1998_2012[1:(floor(nrow(topix_1998_2012)/10)*10),]
topix_1998_2012$by5index=floor(0:(nrow(topix_1998_2012)-1)/10)

topixby10sum=aggregate(topix_1998_2012[,c("return")],by=list(topix_1998_2012$by5index),FUN=sum)
topixby10sd=aggregate(topix_1998_2012[,c("return")],by=list(topix_1998_2012$by5index),FUN=square_sum)

topix_1998_2012_by10=data.frame(list(sum=topixby10sum[[2]],sd=topixby10sd[[2]]))
write.csv(topix_1998_2012_by10,"topix_1998_2012_by10_368.csv")

topix_1998_2012_by10_percent=topix_1998_2012_by10*100
write.csv(topix_1998_2012_by10_percent,"topix_1998_2012_by10_368_percent.csv")

#topix 2003-2012
topix_2003_2012=topix[topix$year%in%(2003:2012),]
topix_2003_2012=topix_2003_2012[1:(floor(nrow(topix_2003_2012)/5)*5),]
topix_2003_2012$by5index=floor(0:(nrow(topix_2003_2012)-1)/5)

topixby5sum=aggregate(topix_2003_2012[,c("return")],by=list(topix_2003_2012$by5index),FUN=sum)
topixby5sd=aggregate(topix_2003_2012[,c("return")],by=list(topix_2003_2012$by5index),FUN=square_sum)

topix_2003_2012_by5=data.frame(list(sum=topixby5sum[[2]],sd=topixby5sd[[2]]))
write.csv(topix_2003_2012_by5,"topix_2003_2012_by5_491.csv")




sp500=read.csv("OxfordManRealizedVolatilityIndices.csv")

usecol=c("DateID","SPX2.rv","SPX2.rk","SPX2.r","SPX2.rv10")
sp500selected=sp500[,usecol]
sp500withoutNA=na.omit(sp500selected)

write.csv(sp500withoutNA,"SP500.csv")

sp500_2007_2009=sp500withoutNA[as.numeric(substr(sp500withoutNA$DateID,1,4))%in%(2007:2009),]
write.csv(sp500_2007_2009$SPX2.r,"sp500_2007_2009_749.csv")
write.csv(sp500_2007_2009,"sp500_2007_2009_true.csv")