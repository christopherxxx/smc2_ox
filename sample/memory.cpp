#include "oxexport.h"
#include "stdio.h"
#include <boost/property_tree/ptree.hpp> 

extern "C" void OXCALL FnMemory(OxVALUE *rtn, OxVALUE *pv, int cArg);
extern "C" void OXCALL FnMemory2(OxVALUE *rtn, OxVALUE *pv, int cArg);

void property_tree_sample()
{
    boost::property_tree::ptree t;
}


void OXCALL FnMemory(OxVALUE *rtn, OxVALUE *pv, int cArg)
{
    property_tree_sample();
    
    int i, j, c, r;
    OxLibCheckType(OX_INT, pv, 0, 1);    
    r = OxInt(pv, 0);
    c = OxInt(pv, 1);
    OxLibValMatMalloc(rtn, 1, 1);
    
    int adress;
    double* ptr;
    ptr=(double*)malloc(sizeof(double)*1000*1000*200);
    for(i=0;i<100;i++)
	{
	    ptr[i]=i*i;
	}
    
    adress=(int)ptr;
    printf("in c memory %p\n",ptr);

    
    OxMat(rtn,0)[0][0]=adress;
}

void OXCALL FnMemory2(OxVALUE *rtn, OxVALUE *pv, int cArg)
{
    int i, j, c, r;
    OxLibCheckType(OX_INT, pv, 0, 1);

    r = OxInt(pv, 0);

    double *ptr;
    ptr=(double*) r;
    double f=ptr[0];    
    printf("in c memory2 %p\n",ptr);

    for(i=0;i<1;i++)
	{
	    printf("%d",(int)ptr[i]);
	}
    
    free(ptr);
    OxLibValMatMalloc(rtn, 1, 1);
    OxMat(rtn,0)[0][0]=f;
}

