#include "oxexport.h"
void OXCALL FnThrees(OxVALUE *rtn, OxVALUE *pv, int cArg)
{

    int i, j, c, r;

    OxLibCheckType(OX_INT, pv, 0, 1);

    r = OxInt(pv, 0);

    c = OxInt(pv, 1);

    OxLibValMatMalloc(rtn, r, c);

    for (i = 0; i < r; i++)
	for (j = 0; j < c; j++)
	    OxMat(rtn, 0)[i][j] = 3;

}

