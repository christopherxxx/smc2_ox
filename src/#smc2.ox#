#include <oxstd.h>
#include <src/resampling.ox>
#include <src/debug.ox>

/*

class SMC

Virtual class for sequential samplers of state space model
Currently, SMC2 and StaticSOPF inherit this class

The main framework is as follows

#first_step
Initialize sampler
You can set theta if needed

#next_steps
for(t in 0..T)
   update(); // learn new data
   kernel(); // transform, evaluate and accept/reject, and increase Nx if needed
end
You can easily design yourself update part as well as kernel part

*/

class SMC
{
public:
    // ssm is an instance of a class deriving SSM
    SMC(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption);
    ~SMC();
    first_step(const mtheta,const isset);
    next_steps();
    resample_theta();
    resamplex();
    resampletheta();
    virtual kernel(const t);
    update(const t);
    ESS(const weight);
    decl ssm;
    decl Nx,Ntheta;
    decl theta,x,wx,logwtheta,wlogtheta,obs,logwx,wtheta,a;
    decl densprior;
    decl loglike,totalloglike;
    decl dim_obs,dim_params,dim_state;
    decl T;
    decl ess;
    decl ismain;
    static decl count=0;
    //for test
    test_transition(const N);
};

SMC::SMC(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption)
{
    println("smc instance created");
    ssm=statespacemodel;
    Nx=Nofx;
    Ntheta=Noftheta;
    obs=observation;
    ismain=ismainoption;
    T=rows(obs);
    dim_params=ssm.dim_params;
    dim_obs=ssm.dim_obs;
    dim_state=ssm.dim_state;
    x=zeros(Ntheta,Nx*dim_state);
    wx=zeros(Ntheta,Nx);
    wtheta=zeros(Ntheta,1);
    theta=zeros(Ntheta,dim_params);
    wlogtheta=zeros(T,Ntheta);
    loglike=zeros(Ntheta,1);
    totalloglike=zeros(Ntheta,1);
    a=zeros(Ntheta,Nx);
    ess=zeros(T,1);
    densprior=zeros(Ntheta,1);
}

SMC::test_transition(const N)
{
    decl i;

    decl t=timer();
    for(i=0;i<N;i++)
	{
	    ssm.transition(x,obs,theta,1,Nx,Ntheta);
	    println(i);
	}
    decl total=timer()-t;
    println("==benchmark==");
    println("Nx:",Nx,";Ntheta:",Ntheta,";dim_state:",dim_state);
    println(N," trials takes ",total/100,"sec");
    println("1000 trials are estimated to take ",total*10/N ,"sec,",total/(6*N),"min");
    println("10000 trials are estimated to take ",total*100/N ,"sec,",total*10/(6*N),"min,",total/(36*N),"h");
}

SMC::first_step(const mtheta,const isset)
{
    // initialize theta
    if(isset==1)
	{
	    theta=mtheta; // if mtheta is given
	}
    else
	{
	    theta=ssm.ranprior(Ntheta);
	}
    if(ismain==1){savemat("rlib/data/init_theta.csv",theta);}
    
    wtheta=ones(wtheta)/(rows(wtheta)*columns(wtheta)); // snapshot of weights
    logwtheta=reshape(log(wtheta),T,Ntheta); // Note that logwtheta is history of weights
    totalloglike=log(ones(Ntheta,1));
    // initialize x
    x=ssm.gen_first_state(theta,Nx,Ntheta);
    wx=ones(wx)/Nx;
    a=ones(Ntheta,Nx);
    ess=zeros(T,1);
    println("state initialized");
}

SMC::next_steps()
{
    decl t;
    // Note that t begins with 1
    for(t=0;t<T;t++)
	{
	    if(ismain==1){println(t," th observation is being processed");}
	    // subsume new observation and update
	    update(t);

	    // transform theta particles,
	    // evaluate and accept/reject them
	    kernel(t);
	}
    if(ismain==1)
	{
	    savemat("rlib/data/logwtheta.csv",logwtheta);
	    savemat("rlib/data/totalloglike.csv",totalloglike);
	    savemat("rlib/data/theta.csv",theta);
	    savemat("rlib/data/ESS.csv",ess);
	}
}

SMC::update(const t)
{
    decl new_x_and_logw=ssm.transition(x,obs[t][],theta,t,Nx,Ntheta);
    logwx=new_x_and_logw[1];
    logwx=setbounds(logwx,(-1)*exp(300),exp(300));
    wx=exp(logwx);
    loglike=log(aggregater(wx,Nx)/Nx);
    loglike=setbounds(loglike,-2000,2000);
    debug(loglike,0);
    x=new_x_and_logw[0];
    totalloglike+=loglike;
    if(t>0)
	{
	    logwtheta[t][] = logwtheta[t-1][]+loglike;
	}
    // when t = 0
    else
	{
	    // logwtheta is supposed to be filled with equal weights in init state
	    logwtheta[t][] = logwtheta[t][]+loglike;
	}
    // sampling index of x
    resamplex();
}

SMC::kernel(const t)
{
}


SMC::resampletheta()
{
    decl selected_theta=resampling2(exp(totalloglike),Ntheta);
    theta=theta[selected_theta][];
    totalloglike=totalloglike[selected_theta][];
    x=x[selected_theta][];
}

SMC::resamplex()
{
    a = resampling2D(wx,Nx,0);
    // flatten x to easily select index
    decl NthetaNx_dimx=reshape(x,Ntheta*Nx,dim_state);
    x=reshape(NthetaNx_dimx[reshape(a,Nx*Ntheta,1)][],Ntheta,Nx*dim_state);
}

SMC::ESS(const weight)
{
    decl vec_weight=vec(weight);
    decl norm_weight = vec_weight / sumc(vec_weight);
    decl sqweight = norm_weight .* norm_weight;
    return 1 / sumc(sqweight);    
}
/*
class StaticSOPF
(plain SMC)
*/

class StaticSOPF : public SMC
{
    StaticSOPF(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption);
    ~StaticSOPF();
    kernel(const t);
};

StaticSOPF::StaticSOPF(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption)
{
    // call constructor of the super class, SMC
    SMC(statespacemodel,observation,Nofx,Noftheta,ismainoption);
}

StaticSOPF::~StaticSOPF()
{
}


StaticSOPF::kernel(const t)
{
    // do nothing
}

    

/*
class PMCMC

PMCMC allowing parallel sampling designed for SMC2
Ntheta>0, iteration=1 and init_tehta=current_theta when used in SMC2
Inside PMCMC, StaticSOPF is called

*/
class PMCMC
{
public:
    PMCMC(statespacemodel,const observation,const Nofx,const iter,const initial_theta,const initial_x,const totalloglikelihood);
    ~PMCMC();
    first_step();
    next_steps();
    decl ssm;
    decl Nx,Ntheta;
    decl dim_state,dim_params,dim_obs;
    decl theta,x,wx,obs,totalloglike,a;
    decl init_theta,init_x;
    decl iteration;
    decl path;
    decl densprior;
    decl T;
};

// init_theta is sampling from posterior distribution
PMCMC::PMCMC(statespacemodel,const observation,const Nofx,const iter,const initial_theta,const initial_x,const totalloglikelihood)
{
    ssm=statespacemodel;
    Nx=Nofx;
    dim_state=ssm.dim_state;
    dim_obs=ssm.dim_obs;
    dim_params=ssm.dim_params;
    obs=observation;
    Ntheta=rows(initial_theta);
    iteration=iter;
    init_theta=initial_theta;
    Ntheta=rows(init_theta);
    x=zeros(Ntheta,Nx);
    path=zeros(iteration,dim_params*Ntheta);
    init_x=initial_x;
    totalloglike=totalloglikelihood;
    T=rows(obs);
}

PMCMC::~PMCMC()
{
}

PMCMC::first_step()
{
    // initialize x
    x=init_x;
    theta=init_theta;
    densprior=ssm.densprior(theta);
    
    println("state initialized");
}

PMCMC::next_steps()
{
    decl i;
    for(i=0;i<iteration;i++)
	{
	    // propose thetastar
	    decl thetastar=ssm.proposal(theta);
	    
	    // simulate x and calc likelihood for each theta using StaticSOPF
	    decl sopf=new StaticSOPF(ssm,obs,Nx,Ntheta,0);
	    sopf.first_step(thetastar,1);
	    sopf.next_steps();
	    
	    decl proposed_totalloglike=sopf.totalloglike;
	    decl proposed_x=sopf.x;
	    decl proposed_densprior=ssm.densprior(thetastar);
	    
	    // accept/reject
	    // currently suppose that kernel is independent
	    decl proposed_logomega=proposed_totalloglike+proposed_densprior;
	    
	    decl logomega=totalloglike+densprior;
	    decl unif=ranu(Ntheta,1);
	    decl accepted=(unif.<(proposed_logomega-logomega));
	    decl rejected=(unif.>=(proposed_logomega-logomega));
	    
	    // replace accepted values
	    theta=selectifr(thetastar,accepted)|selectifr(theta,rejected);
	    densprior=selectifr(proposed_densprior,accepted)|selectifr(densprior,rejected);
	    totalloglike=selectifr(proposed_totalloglike,accepted)|selectifr(totalloglike,rejected);
	    x=selectifr(proposed_x,accepted)|selectifr(x,rejected);	    
	    delete sopf;
	}
}


/*
class SMC2

SMC2 inherits SMC as well as call PMCMCK in its kernel function

update function is the same as super class's
kernel function calls PMCMCK with obs[0:t][] and current theta if ESS condition is satisfied

*/

class SMC2 : public SMC
{
    SMC2(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption);
    ~SMC2();
    kernel(const t);
};

SMC2::SMC2(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption)
{
    // call constructor of the super class, SMC
    SMC(statespacemodel,observation,Nofx,Noftheta,ismainoption);
}

SMC2::kernel(const t)
{
    ess[t]=ESS(exp(totalloglike));
    //if((ess[t]/Ntheta)>10)
    if(ranu(1,1)<0.5)
	{
	    resampletheta();
	    println("PMCMC step start");
	    decl pmcmc = new PMCMC(ssm,obs[0:t][],Nx,1,theta,x,totalloglike);
	    pmcmc.first_step();
	    pmcmc.next_steps();
	    theta=pmcmc.theta;
	    x=pmcmc.x;
	    totalloglike=pmcmc.totalloglike;
	    println("PMCMC step end");
	    delete pmcmc;
	}    


}
