#include <oxstd.h>
#include <src/sopf.ox>

/*
class PMCMC

PMCMC allowing parallel sampling designed for SMC2
Ntheta>0, iteration=1 and init_tehta=current_theta when used in SMC2
Inside PMCMC, StaticSOPF is called

*/
class PMCMC
{
public:
    PMCMC(statespacemodel,const observation,const Nofx,const iter,const initial_theta,const initial_x,const totalloglikelihood,const initial_trees,const adr,const init_logZKT,const init_log_discount_factor);
    ~PMCMC();
    set_window(const window_size);
    first_step();
    next_steps();
    set_tree_adr(const adr);
    decl ssm;
    decl Nx,Ntheta;
    decl dim_state,dim_params,dim_obs;
    decl theta,x,wx,obs,totalloglike,a;
    decl init_theta,init_x,init_trees;
    decl iteration;
    decl path;
    decl densprior;
    decl T;
    decl arate;
    decl tree_adr;
    decl trees;
    decl logZKT,log_discount_factor;
    decl ws;
};

// init_theta is sampling from posterior distribution
PMCMC::PMCMC(statespacemodel,const observation,const Nofx,const iter,const initial_theta,const initial_x,const totalloglikelihood,const inital_trees,const adr,const init_logZKT,const init_log_discount_factor)
{
    ssm=statespacemodel;
    Nx=Nofx;
    dim_state=ssm.dim_state;
    dim_obs=ssm.dim_obs;
    dim_params=ssm.dim_params;
    obs=observation;
    Ntheta=rows(initial_theta);
    iteration=iter;
    init_theta=initial_theta;
    Ntheta=rows(init_theta);
    path=zeros(iteration*Ntheta,dim_params);
    init_x=initial_x;
    init_trees=inital_trees;
    totalloglike=totalloglikelihood;
    T=rows(obs);
    arate=0;
    tree_adr=adr;
    logZKT=init_logZKT;
    log_discount_factor=init_log_discount_factor;
    ws=T;
}

PMCMC::~PMCMC()
{
}

PMCMC::set_window(const window_size)
{
    ws=window_size;
}


PMCMC::first_step()
{
    // initialize x
    x=init_x;
    trees=init_trees;
    theta=init_theta;
    densprior=ssm.densprior(theta);
}

PMCMC::next_steps()
{
    decl i;
    for(i=0;i<iteration;i++)
	{
	    // propose thetastar
	    decl prop=ssm.proposal(theta);
	    decl thetastar=prop[0];
	    decl log_prop_indicator=prop[1];
	    
	    // simulate x and calc likelihood for each theta using StaticSOPF
	    decl sopf=new StaticSOPF(ssm,obs,Nx,Ntheta,0);
	    sopf.set_window(ws);
	    sopf.set_tree_adr(tree_adr);
	    sopf.first_step(thetastar,1);
	    sopf.next_steps();
	    
	    decl proposed_totalloglike=sopf.totalloglike;
	    decl proposed_x=sopf.x;
	    decl proposed_densprior=ssm.densprior(thetastar);
	    decl proposed_trees=sopf.trees;
	    decl proposed_log_discount_factor=sopf.log_discount_factor;
	    decl proposed_logZKT=sopf.logZKT;
	    
	    // accept/reject
	    // currently suppose that kernel is independent
	    decl proposed_logomega=proposed_totalloglike+proposed_densprior;
	    decl logomega=totalloglike+densprior;


	    decl logunif=log(ranu(Ntheta,1));

	    //println(proposed_totalloglike~totalloglike);
	    //println(thetastar|theta);
	    //println(logunif);
	    
	    decl accepted=(logunif.<(proposed_logomega-logomega-log_prop_indicator));
	    decl rejected=(logunif.>=(proposed_logomega-logomega-log_prop_indicator));
	    println(i);

	    arate=sumc(accepted)/Ntheta;
	    
	    println("a-rate:",arate);
	    //println(proposed_totalloglike~totalloglike~thetastar[][0]~theta[][0]~accepted);
	    
	    // replace accepted values
	    decl new_trees=selectifr(proposed_trees,accepted)|selectifr(trees,rejected);	    
	    FreeSIR(tree_adr,selectifr(proposed_trees,rejected)|selectifr(trees,accepted));
	    trees=new_trees;
	    
	    theta=selectifr(thetastar,accepted)|selectifr(theta,rejected);
	    densprior=selectifr(proposed_densprior,accepted)|selectifr(densprior,rejected);
	    totalloglike=selectifr(proposed_totalloglike,accepted)|selectifr(totalloglike,rejected);
	    x=selectifr(proposed_x,accepted)|selectifr(x,rejected);
	    
	    logZKT=selectifr(proposed_logZKT,accepted)|selectifr(logZKT,rejected);
	    log_discount_factor=selectifr(proposed_log_discount_factor,accepted)|selectifr(log_discount_factor,rejected);
	    
	    delete sopf;
	    path[i*Ntheta:(i+1)*Ntheta-1][]=theta;
	}
    savemat("rlib/data/pmcmc_path.csv",path);
    //savemat("rlib/data/x.csv",x);
}
