#include<oxstd.h>
#include<oxprob.h>

extern "resampling,FnDetResampling" DetResampling(const weights,const Nx,const u);

// resampling method using
// Walker's alias method
resampling(weight,ssize)
{
    decl w=vec(weight);
    if(sumc(w)==0)
	{
	    w=ones(w);
	}
    
    w=w/sumc(w);    
    decl nrow=rows(w);
    decl i;
    decl smaller=zeros(nrow,1);
    decl larger=zeros(nrow,1);
    decl threshold=zeros(nrow,1);
    
    // allocationg smaller/larger index    
    decl smaller_than_block_size = vecindex(w.<=(1.0/nrow));
    decl larger_than_block_size = vecindex(w.>(1.0/nrow));
    smaller=reshape(smaller_than_block_size,nrow,1);

    // thresholds are set in block scale
    threshold=reshape(w[smaller_than_block_size],nrow,1)*nrow;    
    
    decl num_allocated=smaller_than_block_size;
    decl current_smaller=rows(smaller_than_block_size); //smallest unallocated block
    decl current_larger=0;
    
    for(i=0;i<rows(larger_than_block_size);i++)
    	{
    	    decl index=larger_than_block_size[i];
    	    decl element_w=w[index]*nrow;
    	    while((current_larger < nrow) && (current_larger < current_smaller) &&(threshold[current_larger]+element_w >=1))
    	    	{		    
    	    	    larger[current_larger]=index;
    	    	    element_w -= (1-threshold[current_larger]);
    	    	    current_larger += 1;
    	    	}
    	    smaller[current_smaller]=index;
	    threshold[current_smaller]=element_w;
    	    current_smaller += 1;
    	}

    //t println("==");
    //t println(threshold ~ (1.-threshold) ~ smaller ~ larger);
    
    // select blocks and choose index
    decl selected_blocks=int(ranu(ssize,1)/(1/nrow));
    decl u=ranu(ssize,1);
    decl s = vecindex(u .< threshold[selected_blocks]);
    decl l = vecindex(u .>= threshold[selected_blocks]);    
    // get index number and access

    if(s==<>)
	{
	    return larger[l];
	}
    else
	{
	    if(l==<>)
		{
		    return smaller[s];
		}
	    else
		{
		    return smaller[s]|larger[l];
		}
	    
	    
	}
}

resampling2(const weights,ssize)
{
    decl w=vec(weights);
    if(sumc(w)==0)
	{
	    w=ones(w);
	}
    w=w/sumc(w);
    decl times = vec(ranmultinomial(ssize,w));
    
    decl result=ones(ssize,1);
    decl i;
    decl counter=0;
    decl maxw=rows(w);
    for(i=0;i<maxw;i++)
	{
	    if(times[i]>0)
		{
		    result[counter:(counter+times[i]-1)]=i;
		    counter += times[i];
		}
	    
	}
    return result;    
}


resampling2D(weight,ssize,direction)
{
    decl result;
    decl i;
    // one row represents weights
    if(direction==0)
	{
	    decl nrow=rows(weight);
	    result=zeros(nrow,ssize);
	    for(i=0;i<nrow;i++)
		{
		    result[i][]=reshape(resampling(weight[i][],ssize),1,ssize);
		}
	}
    // one col represents weights
    else
	{
	    decl ncol=columns(weight);
	    result=zeros(ssize,ncol);
	    for(i=0;i<ncol;i++)
		{
		    result[][i]=reshape(resampling(weight[][i],ssize),ssize,1);
		}
	    
	}
    //println(result);
    //println(weight);
    return result;
}

detresampling(const weights,const ssize)
{
    decl w=vec(weights);
    decl nrow;
    if(sumc(w)==0)
	{
	    w=ones(w);
	}
    if(rows(w)==0)
	{
	    nrow=1;
	}
    else
	{
	    nrow=rows(w);
	}    

    w=w*ssize/sumc(w);
    return DetResampling(w,ssize,ranu(1,1));
}
