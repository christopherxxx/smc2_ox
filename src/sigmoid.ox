#include <oxstd.h>
#ifndef SIGMOID
#define SIGMOID

sigmoid(const x)
{
    if(rows(x)==0)
	{
	    return (1/(1+exp(-x)))*2-1;
	}
    else
	{
	    return (ones(rows(x),columns(x))./(1+exp(-x)))*2-1;
	}
    
}

invsigmoid(const x)
{
    if(rows(x)==0)
	{	    
	    return (-1)*(log((2/(x+1))-1));
	}
    else
	{
	    return (-1)*(log((2*ones(rows(x),columns(x))./(x+1))-1));
	}
    
}
#endif