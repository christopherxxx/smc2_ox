#include <oxstd.h>
#include <src/resampling.ox>

extern "sirtree,FnInitAllSIR" InitAllSIR();
extern "sirtree,FnInitSIR" InitSIR(const tree);
extern "sirtree,FnFreeSIR" FreeSIR(const tree, const index);
extern "sirtree,FnAddSIR" AddSIR(const tree, const index,const t,const mat,const ispruning);
extern "sirtree,FnCopySIR" CopySIR(const tree, const index, const selected);
extern "sirtree,FnTransitionSIR" TransitionSIR(const tree, const index, const start, const end, const logw);
extern "sirtree,FnGraphml" GraphmlSIR(const tree, const index);


/*

class SMC

Virtual class for sequential samplers of state space model
Currently, SMC2 and StaticSOPF inherit this class

The main framework is as follows

#first_step
Initialize sampler
You can set theta if needed

#next_steps
for(t in 0..T)
   update(); // learn new data
   kernel(); // transform, evaluate and accept/reject, and increase Nx if needed
end
You can easily design yourself update part as well as kernel part

*/

class SMC
{
public:
    // ssm is an instance of a class deriving SSM
    SMC(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption);
    ~SMC();
    output(const dirname);
    
    first_step(const mtheta,const isset);
    next_steps();
    resampletheta(const t);
    resamplex(const t);
    set_tree_adr(const adr);
    virtual kernel(const t);
    update(const t);
    ESS(const weight);
    decl ssm;
    decl Nx,Ntheta;
    decl theta,x,wx,logwtheta,obs,logwx,wtheta,a;
    decl densprior,evidence;
    decl loglike,totalloglike;
    decl dim_obs,dim_params,dim_state;
    decl T;
    decl counttime;
    decl init_time;
    decl ess;
    decl ismain;
    decl ws,ss;
    decl w_start,w_end;
    decl log_discount_factor,logZKT;
    decl meantheta;
    decl predx;
    decl init_theta;
    decl tree_adr,trees;
    decl all_evidence;
    
    static decl count=0;
    set_window(const window_size);
    //for test
    test_transition(const N);
};

SMC::SMC(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption)
{
    ssm=statespacemodel;
    Nx=Nofx;
    Ntheta=Noftheta;
    obs=observation;
    ismain=ismainoption;
    T=rows(obs);
    dim_params=ssm.dim_params;
    dim_obs=ssm.dim_obs;
    dim_state=ssm.dim_state;
    x=zeros(Ntheta,Nx*dim_state);
    wx=zeros(Ntheta,Nx);
    wtheta=zeros(Ntheta,1);
    theta=zeros(Ntheta,dim_params);
    loglike=zeros(Ntheta,1);
    totalloglike=zeros(Ntheta,1);
    a=zeros(Ntheta,Nx);
    logZKT=zeros(Ntheta,T);
    ess=zeros(T,1);
    densprior=zeros(Ntheta,1);
    evidence=zeros(T,1);
    counttime=zeros(T,1);
    init_time=timer();
    meantheta=zeros(T,dim_params);
    predx=zeros(T,dim_state);
    tree_adr=InitAllSIR();
    all_evidence=zeros(T,T);
    
    trees=zeros(Ntheta,1);
    ws=T;
    ss=1;
}

SMC::set_tree_adr(const adr)
{
    tree_adr=adr;
}


SMC::test_transition(const N)
{
    decl i;

    decl t=timer();
    for(i=0;i<N;i++)
	{
	    ssm.transition(x,obs,theta,1,Nx,Ntheta,0,0,0);
	    println(i);
	}
    decl total=timer()-t;
    println("==benchmark==");
    println("Nx:",Nx,";Ntheta:",Ntheta,";dim_state:",dim_state);
    println(N," trials takes ",total/100,"sec");
    println("1000 trials are estimated to take ",total*10/N ,"sec,",total/(6*N),"min");
    println("10000 trials are estimated to take ",total*100/N ,"sec,",total*10/(6*N),"min,",total/(36*N),"h");
}

SMC::first_step(const mtheta,const isset)
{
    // initialize theta
    if(isset==1)
	{
	    theta=mtheta; // if mtheta is given
	}
    else
	{
	    theta=ssm.ranprior(Ntheta);
	}
    init_theta=theta;
    
    wtheta=ones(wtheta)/(rows(wtheta)*columns(wtheta)); // snapshot of weights
    logwtheta=reshape(log(wtheta),T,Ntheta); // Note that logwtheta is history of weights
    totalloglike=log(ones(Ntheta,1));
    // initialize x
    x=ssm.gen_first_state(theta,Nx,Ntheta);
    wx=ones(wx)/Nx;
    
    decl i;
    for(i=0;i<Ntheta;i++)
	{
	    trees[i]=InitSIR(tree_adr);
	    AddSIR(tree_adr,trees[i],0,zeros(Nx,1),1); // now t=-1, creating root of proposals at t=0
	}
    log_discount_factor=zeros(Ntheta,1);
    
    a=ones(Ntheta,Nx);
    ess=zeros(T,1);
}

SMC::set_window(const window_size)
{
    if(window_size>T)
	{
	    println("invalid window size");
	}
    else
	{
	    ws=window_size;
	    
	}
}


SMC::next_steps()
{
    decl t;
    w_start=0;
    w_end=0;
    
    // Note that t begins with 1
    for(t=0;t<T;t++)
	{
	    // position of the window
	    w_end=t; // change the end point
	    
	    if(ismain==1){println(t," th observation is being processed");}
	    // subsume new observation and update
	    update(t);
	    
	    // transform theta particles,
	    // evaluate and accept/reject them
	    kernel(t);
	    
	    counttime[t]=timer()-init_time;
		
	}
    if(ismain==1)
	{

	}
}

SMC::update(const t)
{    

    decl new_x_and_logw;
    decl window_start=t-ws+1;
    decl start=max(window_start,0);
    
    new_x_and_logw=ssm.transition(x,obs[t][],theta,t,Nx,Ntheta,tree_adr,trees,1,start);

    // set new x
    x=new_x_and_logw[0];    
    
    //ZKT
    decl log_xi_unweighted_sum_weight=new_x_and_logw[2];
    decl prev_log_discount_factor=log_discount_factor;
    log_discount_factor=new_x_and_logw[3];

    // zkt k=window_start..t
    //logzkt[theta_index][k] is logz_{k:t}
    //logzkt[theta_index][t] is for the window from t to t i.e. learning only y_t

    if(start>0)
	{
	    prev_log_discount_factor=prev_log_discount_factor[][1:t-start+1];
	}
    decl logzkt=log_xi_unweighted_sum_weight+prev_log_discount_factor-reshape(log(Nx),Ntheta,t-start+1);

    if(start==0)
	{
	    logzkt=logzkt~zeros(Ntheta,T-t-1);
	}
    else
	{
	    logzkt=reshape(.Inf,Ntheta,start)~logzkt~zeros(Ntheta,T-t-1);
	}    
    
    decl logZ_k_minus_t_minus;
    if(window_start>0)
	{
	    logZ_k_minus_t_minus=logZKT[][window_start-1];;
	}
    logZKT=logZKT+logzkt;
    
    // processes for specific x
    logwx=new_x_and_logw[1];
    logwx=setbounds(logwx,(-1)*exp(200),exp(200));
    wx=exp(logwx);

    // processes for accumulated x
    if(window_start>0)
	{
	    // Rolling SMC2
	    decl log_updation_ratio=logZKT[][window_start]-logZ_k_minus_t_minus;
	    logwtheta[t][]=logwtheta[t-1][]+log_updation_ratio';
	    totalloglike=logZKT[][window_start];
	    loglike=logzkt[][window_start];
	}
    else
	{
	    // normal SMC2
	    loglike=log(aggregater(wx,Nx)/Nx);
	    loglike=setbounds(loglike,-2000,2000);
	    totalloglike+=loglike;
	    if(t>0)
		{
		    logwtheta[t][] = logwtheta[t-1][]+loglike';	    
		}
	    else
		{
		    logwtheta[t][] = logwtheta[t][]+loglike';
		}
	}
    
    // general
    
    //println(totalloglike~logZKT[][0]);
    
    //println(theta);
    //println(totalloglike');

    // before resampling of x
    decl broadx;
    decl avweight;
    decl avweight_cur;
    if(t>0)
	{
	    avweight=exp(logwtheta[t-1][]'-max(logwtheta[t-1][]'))/sumc(exp(logwtheta[t-1][]'-max(logwtheta[t-1][]')));
	    avweight_cur=exp(logwtheta[t][]'-max(logwtheta[t][]'))/sumc(exp(logwtheta[t][]'-max(logwtheta[t][]')));

	}    
    // when t = 0
    else
	{
	    // logwtheta is supposed to be filled with equal weights in init state
	    avweight=exp(logwtheta[t][]')/sumc(exp(logwtheta[t][]'));
	    avweight_cur=exp(logwtheta[t][]')/sumc(exp(logwtheta[t][]'));
	}
    evidence[t]=sumc(exp(loglike).*avweight);
    all_evidence[t][start:t]=sumc(exp(logzkt[][start:t]).*avweight);
    
    broadx=avweight'*x;
    meantheta[t][]=avweight_cur'*theta;

    // output current theta
    if(ismain==1){println(meantheta[t][]);}
    

    decl i;
    for(i=0;i<dim_state;i++)
	{
	    predx[t][i]=sumr(broadx[][i+dim_state*range(0,Nx-1)])/Nx;
	}
    
    // sampling index of x
    resamplex(t);
}

SMC::kernel(const t)
{
    
}


SMC::resampletheta(const t)
{
    decl resample_prob=exp(logwtheta[t][]-max(logwtheta[t][])); // rescaling for safety
    decl selected_theta=resampling2(resample_prob,Ntheta);
    
    decl new_trees=CopySIR(tree_adr,trees,selected_theta);    
    FreeSIR(tree_adr,trees);
    trees=new_trees;

    theta=theta[selected_theta][];
    totalloglike=totalloglike[selected_theta];
    x=x[selected_theta][];
    logwtheta[t][]=0;
    
    logZKT=logZKT[selected_theta][];
    log_discount_factor=log_discount_factor[selected_theta][];
}

SMC::resamplex(const t)
{
    decl i=0;
    for(i=0;i<Ntheta;i++)
	{
	    decl a=resampling2(wx[i][],Nx);	    
	    AddSIR(tree_adr,trees[i],t+1,vec(a),imod(t,3)==0);
	    a=a.*ones(Nx,dim_state)*dim_state+reshape(range(0,dim_state-1)',Nx,dim_state);
	    a=reshape(a,Nx*dim_state,1)';
	    x[i][]=x[i][a];
	}
}

SMC::ESS(const weight)
{
    decl vec_weight=vecr(weight);
    decl norm_weight = vec_weight / sumc(vec_weight);
    //print("vec_weight",(vec_weight));
    decl sqweight = norm_weight .* norm_weight;
    return 1 / sumc(sqweight);
}


SMC::output(const dirname)
{
    decl dname=sprint("rlib/data/output/",dirname);
    systemcall(sprint("mkdir ",dname));
    
    dname=sprint(dname,"/");
    ssm.output_paraminfo(sprint(dname,"paraminfo.csv"));
    savemat(sprint(dname,"obs.csv"),obs);
    savemat(sprint(dname,"logwx.csv"),logwx);
    savemat(sprint(dname,"x.csv"),x);
    savemat(sprint(dname,"theta.csv"),theta);
    savemat(sprint(dname,"logwtheta.csv"),logwtheta);
    savemat(sprint(dname,"totalloglike.csv"),totalloglike);
    savemat(sprint(dname,"theta.csv"),theta);
    savemat(sprint(dname,"ESS.csv"),ess);
    savemat(sprint(dname,"evidence.csv"),evidence);
    savemat(sprint(dname,"time.csv"),counttime);
    savemat(sprint(dname,"predx.csv"),predx);
    savemat(sprint(dname,"meantheta.csv"),meantheta);
    savemat(sprint(dname,"init_theta.csv"),init_theta);
    savemat(sprint(dname,"all_evidence.csv"),all_evidence);
}

SMC::~SMC()
{

}
