#include <oxstd.h>
#include <src/pmcmc.ox>

/*
class SMC2

SMC2 inherits SMC as well as call PMCMCK in its kernel function

update function is the same as super class's
kernel function calls PMCMCK with obs[0:t][] and current theta if ESS condition is satisfied

*/

class SMC2 : public SMC
{
    SMC2(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption);
    ~SMC2();
    output_arate(const dirname);
    set_ess_th(const th);
    kernel(const t);
    decl ess_th;
    decl arate;
};

SMC2::SMC2(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption)
{
    // call constructor of the super class, SMC
    SMC(statespacemodel,observation,Nofx,Noftheta,ismainoption);
    arate=zeros(T,1);
    ess_th=0.5;
}

SMC2::kernel(const t)
{
    decl ess_prob=exp(logwtheta[t][]-max(logwtheta[t][]));
    ess[t]=ESS(ess_prob);
    println("ess_rate:",ess[t]/Ntheta);

    if((ess[t]/Ntheta < ess_th) || t==(T-1))
	{
	    println("PMCMC");
	    resampletheta(t);
	    decl pmcmc = new PMCMC(ssm,obs[0:t][],Nx,1,theta,x,totalloglike,trees,tree_adr,logZKT[][0:t],log_discount_factor);
	    pmcmc.set_window(min(ws,t+1));
	    pmcmc.first_step();
	    pmcmc.next_steps();
	    theta=pmcmc.theta;
	    trees=pmcmc.trees;
	    x=pmcmc.x;
	    totalloglike=pmcmc.totalloglike;
	    arate[t]=pmcmc.arate;
	    logZKT=pmcmc.logZKT~zeros(Ntheta,T-t-1);
	    log_discount_factor=pmcmc.log_discount_factor;
	    delete pmcmc;
	    //ess[t]=ESS(exp(logwtheta[t][]));
	}
}
SMC2::set_ess_th(const th)
{
    ess_th=th;
}


SMC2::~SMC2()
{
    ~SMC();
}
SMC2::output_arate(const dirname)
{
    decl dname=sprint("rlib/data/output/",dirname);
    systemcall(sprint("mkdir ",dname));
    dname=sprint(dname,"/");
    savemat(sprint(dname,"arate.csv"),arate);
}
