#include <oxstd.h>
#include <src/smc.ox>

/*
class StaticSOPF
(plain SMC)
*/

class StaticSOPF : public SMC
{
    StaticSOPF(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption);
    ~StaticSOPF();
    kernel(const t);
};

StaticSOPF::StaticSOPF(statespacemodel,const observation,const Nofx,const Noftheta,const ismainoption)
{
    // call constructor of the super class, SMC
    SMC(statespacemodel,observation,Nofx,Noftheta,ismainoption);
}

StaticSOPF::~StaticSOPF()
{
    ~SMC();
}


StaticSOPF::kernel(const t)
{
    // do nothing
}
