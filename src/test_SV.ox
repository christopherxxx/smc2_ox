#include<oxstd.h>
#include<src/smc2.ox>
#include<model/SV.ox>

main()
{
    ranseed(3);

    // set parameters of generic data
    decl v=new SV();
    decl mu=-9;
    decl smphi=invsigmoid(0.9);
    
    decl logsigeta=0.175;
    decl smrho=invsigmoid(-0.5);
    decl params=mu|smphi|logsigeta|smrho;
    
    // gen synthetic data
    decl result=v.synthetic(params,200);
    decl obs=result[1];
    decl hidden=result[0];
    savemat("rlib/data/test_sv_obs.csv",obs);
    savemat("rlib/data/test_sv_state.csv",hidden);
    savemat("rlib/data/test_sv_true.csv",params);
    v.output_paraminfo("rlib/data/test_sv_paraminfo.csv");
    decl Nx=10;
    decl Ntheta=50;
    decl i;

    decl topix=loadmat("rlib/topix_return.csv")[][1];
    decl topix_week=aggregatec(topix,5);
    topix_week=topix_week[0:rows(topix_week)-2];
    println(rows(topix_week));
    println(rows(topix));
    
    decl data;

    data=topix_week[0:20];
    //data=topix[0:500];
    //data=obs;

    savemat("rlib/data/data_used.csv",data);
    
    ranseed(2);
    // trial of smc2
    for(i=0;i<1;i++)
	{	    
	    println(i);
	    decl smc2 = new SMC2(v,data,Nx,Ntheta,1);
	    smc2.set_window(10);
	    smc2.first_step(0,0);
	    smc2.next_steps();
	    smc2.output("sample");
	    smc2.output_arate("sample");
	    delete smc2;
	}
    //decl smc2 = new SMC2(v,obs,Nx,Ntheta,1);
    //smc2.first_step(0,0);
    //smc2.test_transition(10);
    println("end of the program");
}
