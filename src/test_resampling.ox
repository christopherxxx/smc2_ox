#include<oxstd.h>
#include<src/resampling.ox>

main()
{
    decl i;
    decl Nx=5;
    decl Ntheta=5;
    decl N=1;
     decl w=reshape(range(0,Ntheta*Nx-1)*10,Ntheta,Nx);
     //decl w=ranu(Ntheta,Nx);
    decl t=timer();
    for(i=0;i<N;i++)
	{
	    println(i);
	    println(w);
	    decl result=resampling2D(w,40,0);
	    println(result);
	    
	}
    decl sec =(timer()-t)/100;
    println(N," iteration takes ",sec," sec");
    println("1000 iterations are estimated to take ",sec*(1000/N)," sec ",sec*(1000/N)/60," min ");
}
