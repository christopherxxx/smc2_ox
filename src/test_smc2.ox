#include<oxstd.h>
#include<src/smc2.ox>
#include<model/VAR.ox>

main()
{
    ranseed(0);

    // set parameters of generic data
    decl dim=1;decl alpha=<1>;decl Phi=<0.5>;
    //decl dim=2;decl alpha=<1,1>;decl Phi=<0.5,0.00;0.00,0.5>;
    decl SigEtaL=invsigmoid(1*ranu(dim,dim));
    decl logSigEtaD=log(0.5*ranu(dim,1));
    decl SigEpsL=invsigmoid(1*ranu(dim,dim));
    decl logSigEpsD=log(2*ranu(dim,1));
    decl mu=zeros(dim,1);
    decl logsig2=log(ones(dim,1));
    decl v=new VAR(dim);
    decl params=vecr(alpha)|vecr(Phi)|vecr(SigEtaL)|vecr(logSigEtaD)|vecr(SigEpsL)|vecr(logSigEpsD)|vecr(mu)|vecr(logsig2);

    // gen synthetic data
    decl result=v.synthetic(params,20);
    decl obs=result[1];
    decl hidden=result[0];
    
    savemat("rlib/data/test_smc2.csv",obs);
    savemat("rlib/data/test_smc2_state.csv",hidden);
    savemat("rlib/data/test_smc2_true.csv",params);
    v.output_paraminfo("rlib/data/test_smc2_paraminfo.csv");
    decl Nx=500;
    decl Ntheta=200;
    decl i;

    ranseed(1);
    // trial of smc2
    for(i=0;i<1;i++)
	{	    
	    println(i);
	    decl smc2 = new SMC2(v,obs,Nx,Ntheta,1);
	    // mtheta is unset
	    smc2.first_step(0,0);
	    smc2.next_steps();
	}
    //decl smc2 = new SMC2(v,obs,Nx,Ntheta,1);
    //smc2.first_step(0,0);
    //smc2.test_transition(10);
    println("end of the program");
}
