#include<oxstd.h>
#include<src/smc2.ox>
#include<model/modSV.ox>
#include<model/modSVwithoutL.ox>
#include<model/modTSV.ox>

main()
{
    ranseed(3);
    // general settings
    decl header="HOMEmodSP500";
    decl Nx=300;
    decl Ntheta=1000;
    decl ess_th=0.4;
    decl ws=1000;
    decl modelname="SV";
    decl T=1983;
    decl sim=0;
    decl isdep=1; //meaningless

    decl dirname=sprint(header,modelname,"_","Nx",Nx,"Ntheta",Ntheta,"Sim",sim,"T",T,"W",ws,"Dep",isdep);
    println(dirname);
    
    // params
    decl mu=-9;
    decl smphi=0.9;
    decl logsigeta=0.175;
    decl smrho=0.5;
    decl logdf=log(10);

    decl params;
    decl model;
    if(modelname=="SVwithoutL")
	{
	    println("SV without leverage");
	    model=new SVwithoutL();
	    params=mu|smphi|logsigeta;
	}
    else
	{
	    if(modelname=="TSV")
		{
		    println("t-distributed SV");
		    params=mu|smphi|logsigeta|logdf;
		    model=new TSV();
		}
	    else
		{
		    
		    println("SV with leverage");
		    params=mu|smphi|logsigeta|smrho;
		    model=new SV();
		    println(model.densprior(params'));
		}
	    
	}

    decl data;
    if(sim)
	{
	    println("sim");
	    // gen synthetic data
	    decl result=model.synthetic(params,T);
	    decl obs=result[1];
	    decl hidden=result[0];
	    savemat("sample.csv",hidden);
	    data=obs[0:T-1];
	}
    else
	{
	    println("emp");
	    decl sp500_2006_2013=loadmat("rlib/sp500_2006_2013_1983.csv")[][1];
	    //decl topixby5_2003_2012=loadmat("rlib/topix_2003_2012_by5_491.csv")[][1];
	    data=sp500_2006_2013[0:T-1];
	}
    
    // trial of smc2
    model.set_proposal(isdep);
    decl smc2 = new SMC2(model,data,Nx,Ntheta,1);
    smc2.set_window(ws);
    smc2.set_ess_th(ess_th);
    smc2.first_step(0,0);
    smc2.next_steps();
    smc2.output(dirname);
    smc2.output_arate(dirname);
    delete smc2;
	    
    println("end of the program");
}
